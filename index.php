<?php include_once('header.php'); ?>

    <div id="main-wrapper">
        <div id="main" class="clearfix">
            <div id="content" class="column">
                <div class="section">
                    <section id="section-content" class="section section-content">
                        <div class="jp-audio">
                            <div class="jp-type-playlist">

                                <div class="above-jplayer intro">
                                        <h2>Devonport Guildhall Audio Tour</h2>
                                        <p style="padding-top:0">Welcome to the Devonport Guildhall audio tour. This tour was developed for individuals to use whilst exploring the Guildhall and outside area. It provides insight into the various areas of historical interest on this site with contributions from local people, professional historials and others. To get started and to better prepare yourself for the tour experience tap or click on the Audio Tour Introduction link below.</p>
                                        <p>This tour is best experienced through headphones. If you don’t have a pair with you you can purchase a pair from the Column Bakehouse.</p>
                                        <p>If during your visit you would like to take the tour up to the top of the Devonport Column you will need a ticket. These can also be purchased from the Column Bakehouse.</p>
                                </div>

                                <div class="intro-box-container">
                                    <a href="intro1.php">
                                        <div class="intro-box-title">
                                            Audio Tour Introduction <img src="img/right.gif">
                                        </div>
                                    </a>
                                    <div class="intro-box">
                                        <p>This part of the audio tour introduces you to how the tour works and prepares you for the experience.</p>
                                        <p>Approximate running time: 03:00 mins</p>
                                    </div>
                                </div>

                                <div class="intro-box-container">
                                    <a href="in1.php">
                                        <div class="intro-box-title">
                                            Inside Tour <img src="img/right.gif">
                                        </div>
                                    </a>
                                    <div class="intro-box">
                                        <p>This part of the audio tour takes you on a journey around the inside of the historic Guildhall and provides insights into the exciting features and historical uses of this amazing building.</p>
                                        <p>Approximate running time: 29:00 mins</p>
                                    </div>
                                </div>

                                <div class="intro-box-container">
                                    <a href="out1.php">
                                        <div class="intro-box-title">
                                            Outside Tour <img src="img/right.gif">
                                        </div>
                                    </a>
                                    <div class="intro-box">
                                        <p>This part of the audio tour takes you on a journey outside the Guildhall and introduces you to the historic and architectural features of the surrounding area. The optional last section of this tour includes a trip up the Devonport Column, for which you will need to purchase tickets.</p>
                                        <p>Approximate running time: 32:00 mins</p>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <!-- /.section, /#content -->



        </div>
    </div>


<?php include_once('footer.php'); ?>