/**
 * jQuery Cookie plugin
 *
 * Copyright (c) 2010 Klaus Hartl (stilbuero.de)
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 *
 */
jQuery.cookie = function (key, value, options) {

    // key and at least value given, set cookie...
    if (arguments.length > 1 && String(value) !== "[object Object]") {
        options = jQuery.extend({}, options);

        if (value === null || value === undefined) {
            options.expires = -1;
        }

        if (typeof options.expires === 'number') {
            var days = options.expires, t = options.expires = new Date();
            t.setDate(t.getDate() + days);
        }

        value = String(value);

        return (document.cookie = [
            encodeURIComponent(key), '=',
            options.raw ? value : encodeURIComponent(value),
            options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
            options.path ? '; path=' + options.path : '',
            options.domain ? '; domain=' + options.domain : '',
            options.secure ? '; secure' : ''
        ].join(''));
    }

    // key and possibly options given, get cookie...
    options = value || {};
    var result, decode = options.raw ? function (s) { return s; } : decodeURIComponent;
    return (result = new RegExp('(?:^|; )' + encodeURIComponent(key) + '=([^;]*)').exec(document.cookie)) ? decode(result[1]) : null;
};
;
/*global jQuery */
/*! 
* FitVids 1.0
*
* Copyright 2011, Chris Coyier - http://css-tricks.com + Dave Rupert - http://daverupert.com
* Credit to Thierry Koblentz - http://www.alistapart.com/articles/creating-intrinsic-ratios-for-video/
* Released under the WTFPL license - http://sam.zoy.org/wtfpl/
*
* Date: Thu Sept 01 18:00:00 2011 -0500
*/

(function( $ ){

  $.fn.fitVids = function( options ) {
    var settings = {
      customSelector: null
    }
    
    var div = document.createElement('div'),
        ref = document.getElementsByTagName('base')[0] || document.getElementsByTagName('script')[0];
        
    div.className = 'fit-vids-style';
    div.innerHTML = '&shy;<style>         \
      .fluid-width-video-wrapper {        \
         width: 100%;                     \
         position: relative;              \
         padding: 0;                      \
      }                                   \
                                          \
      .fluid-width-video-wrapper iframe,  \
      .fluid-width-video-wrapper object,  \
      .fluid-width-video-wrapper embed {  \
         position: absolute;              \
         right: 0;                        \
         bottom: 0;                       \
         top: 0;                          \
         left: 0;                         \
         width: 100%;                     \
         height: 100%;                    \
      }                                   \
    </style>';
                      
    ref.parentNode.insertBefore(div,ref);
    
    if ( options ) { 
      $.extend( settings, options );
    }
    
    return this.each(function(){
      var selectors = [
        "iframe[src*='player.vimeo.com']", 
        "iframe[src*='www.youtube.com']",  
        "iframe[src*='www.kickstarter.com']", 
        "object", 
        "embed"
      ];
      
      if (settings.customSelector) {
        selectors.push(settings.customSelector);
      }
      
      var $allVideos = $(this).find(selectors.join(','));

      $allVideos.each(function(){
        var $this = $(this);
        if (this.tagName.toLowerCase() == 'embed' && $this.parent('object').length || $this.parent('.fluid-width-video-wrapper').length) { return; } 
        var height = this.tagName.toLowerCase() == 'object' ? $this.attr('height') : $this.height(),
            aspectRatio = height / $this.width();
        if(!$this.attr('id')){
          var videoID = 'fitvid' + Math.floor(Math.random()*999999);
          $this.attr('id', videoID);
        }
        $this.wrap('<div class="fluid-width-video-wrapper"></div>').parent('.fluid-width-video-wrapper').css('padding-top', (aspectRatio * 100)+"%");
        $this.removeAttr('height').removeAttr('width');
      });
    });
  
  }
})( jQuery );;
(function($) {
  /// Liam's Filter Javascript revamped a little. This refactor has been 
  /// tested on the front end via FireBug on the Dev environment, due to  
  /// not having facets running locally.
  //TODO This script has no effect at present as it relies on summing the 
  // heights of elements which are hidden at the time the script is called.
  // Either the script needs to be invoked at a point when the facet lists 
  // are displayed, or else the facet lists need to be displayed 'off-screen'
  // by default (e.g. using a large negative 'left' property) rather than hidden.
  $.fn.applyRevealMoreFilters = function(){
    /// create our reveal function - responsible for the
    /// toggling between totalheight and limitheight.
    var reveal = function(e){
      var gui = {}, metrics;
      gui.self = $(e.target).closest('div');
      gui.ui = gui.self.siblings('ul');
      metrics = gui.self.data('reveal-metrics.tate');
      if ( gui.self.is('.collapsed') ) {
        gui.self.removeClass('collapsed').text('Hide');
        gui.ui.animate({
          'height': metrics.totalheight + 'px'
        });
      }
      else {
        gui.self.addClass('collapsed').text('More');
        gui.ui.animate({
          'height': metrics.limitheight + 'px'
        });
      }
      return false;
    };
    /// step each facet area and count the facet items dimensions
    /// this allows us to calculate at what point to do the "reveal".
    this.each(function(){
      var 
        gui = {
          self: $(this),
          ul: $(this).find('ul')
        },
        metrics = {
          totalheight: 0,
          limitheight: 0
        }
      ;
      gui.ul.children('li').each(function(i){
        metrics.totalheight += Math.round($(this).outerHeight());
        if ( metrics.totalheight < 250 ) {
          metrics.limitheight = metrics.totalheight;
        }
        /// if our facet list is larger than our limit then limit the list
        /// and create a more link which will handle the animation..
        else if ( !gui.ul.is('.limited') ) {
          gui.ul.addClass('limited').css({
            'height': metrics.limitheight + 'px'
          });
          $('<a class="view-more-link collapsed">More</a>')
            .data('reveal-metrics.tate', metrics)
            .bind('click.tate', reveal)
            .appendTo(gui.self)
          ;
        }
      });
    });
  };

  /// Hide failed images for Search
  ;(function(){
    var 
      nu = function(){}, 
      rp = 'http://www.tate.org.uk/art/content/images/placeholder-128x128.png',
      sr = $('.search-results')
    ;
    if ( sr.length ) {
      sr.find('img').each(function(){
        if ( this.complete && (!this.width || !this.height) ){
          this.src = rp; }
        else { 
          this.onerror = function(){this.onerror = nu;this.src = rp;}
        }
      });
    }
  })();
  
  /// Grouping for MLT Listings
  $(function(ul, uls){
    ul = $('.block-apachesolr-search ul:not(.mlt-grouped)');
    if ( ul.length ) {
      uls = {};
      ul.find('li>a').each(function(link, grp, txt, p){
        link = $(this); grp = 'Other'; txt = link.text();
        if ( (p = txt.indexOf(':')) != -1 ) {
          grp = txt.substring(0,p);
          link.text(txt.substring(p+1, txt.length));
        }
        if ( !uls[grp] ) {
          uls[grp] = $('<ul class="mlt-grouped" />')
            .insertAfter(ul);
          uls[grp].before('<h3>'+grp+'</h3>');
        }
        uls[grp].append(link.parent());
      });
      ul.remove();
    }
  });

  // on dom ready
  $(function(){
    
    /// Add the dynamic 'More' buttons/links
    $('#block-tate-facet-tate-facet-main .facet-list > li').applyRevealMoreFilters();
    
    $('.quick-link-menu').prependTo('.region-content-inner');
    $('#search-block-form h2.element-invisible').text('Search Tate website')
    $('.front #block-ddblock-35').prepend('<h2 class="block-title">Work of the week</h2>');
    $('.front #block-ddblock-35 .block-inner').css('display' , 'none');
    $('.front #block-ddblock-35 h2').addClass('not-open');
    
    $('.front #block-ddblock-35 h2').click(function(){
      if ($(this).hasClass('open')) {
        $(this).next('div').slideUp('fast');
        $(this).removeClass('open');
      }
      else {
        $(this).next('div').slideDown('fast');
        $(this).addClass('open');
      }
    });
    
    $('.front .quick-link-item h2').click(function() {
      if ($(this).hasClass('open')) {
        $(this).next('ul').slideUp('fast');
        $(this).removeClass('open');
      }
      else {
        $(this).next('ul').slideDown('fast');
        $(this).addClass('open');
      }
    });

    $('.navigation').prepend('<div class="close-slide-menu"></div>');
    $('#block-menu-block-5 ').prepend('<div class="close-slide-menu"></div>');
    $('#search-block-form').prepend('<div class="close-slide-menu"></div>');

    if ($('#block-tate-facet-tate-facet-main').length) {
      $('#block-tate-facet-tate-facet-main').prepend('<div class="close-slide-menu"></div><h2>Filter by:</h2>');
      $('#header').prepend('<div class="facet-handle"><span class="button"></span><span>Browse</span></div>');
    }
        
    if ($('.block-tatesharebuttons').length) {
      $('.block-tatesharebuttons').prepend('<h2>Share</h2>');
    }

    $('.navigation').prependTo('#page');
    $('#block-menu-block-5').appendTo('.navigation');
    $('#search-block-form').appendTo('.navigation');
    $('#block-tate-facet-tate-facet-main').prependTo('#page');
        
    if ( $('.secondary-menu').length ) {
      $('.secondary-menu').prependTo('#main-wrapper');
    }
    
    if ( $('#breadcrumb li a').length ) {
      $('.secondary-menu').css('display' , 'none');
      $('#breadcrumb').prepend('<div class="breadcrumbs-title">Back to:</div>')
    }
    
    if ( $('#region-sidebar-first li.active-trail').length ) {
      $('#region-sidebar-first li').css('display' , 'none');
      $('#region-sidebar-first li.active-trail ul.menu li').css('display' , 'block');
      if ($('#region-sidebar-first li.active-trail li.active-trail').length) {
        $('#region-sidebar-first li.active-trail ul.menu li').css('display' , 'none');
        $('#region-sidebar-first li.active-trail ul.menu li.active-trail ul.menu li').css('display' , 'block');
        if ($('#region-sidebar-first li.active-trail li.active-trail li.active-trail').length) {
          $('#region-sidebar-first li.active-trail ul.menu li.active-trail').css('display' , 'none');
        }
        else {
          $('#region-sidebar-first li.active-trail li.active-menu ul.menu').appendTo('#region-sidebar-first .menu-block-wrapper')
        }
      }
      else {
        $('#region-sidebar-first li.active-trail ul.menu').appendTo('#region-sidebar-first .menu-block-wrapper')
      }
    }

    $("h2:contains('Secondary menu')").css("display", "none");

    $('.main-menu-handle').click(function() {
      $('#search-block-form').css('display' , 'block')
      if ($('.main-menu-handle').hasClass('open')) {
        $('.navigation').slideUp('slow');
        $('.main-menu-handle').removeClass('open')
      }
      else {
        $('.navigation').slideDown('slow');
        $('.main-menu-handle').addClass('open');
      }
    });

    $('.facet-handle').click(function() {
      $('#block-tate-facet-tate-facet-main').slideDown('slow');
    });

    $('.navigation .close-slide-menu').click(function() {
      $('.navigation').slideUp('slow');
      $('.main-menu-handle').removeClass('open');
    });
    
    $('#block-tate-facet-tate-facet-main .close-slide-menu').click(function() {
      $('#block-tate-facet-tate-facet-main').css('display' , 'none');
      $('.main-menu-handle').removeClass('open');
    });
    
    $('#block-tate-blocks-sponsors-lhs').appendTo('#region-content');
    $('#region-sidebar-first').prependTo('#zone-content');
    $('#block-tate-blocks-login-js').appendTo('.login-handle');
    $('.node-type-exhibition #region-sidebar-first').insertAfter('#region-content');
    $('#block-views-event-prices-block-rhs').insertAfter('.view-exhibition-second-title')
    $('.node-type-landing-page.context-whats-on #zone-content').html('<div class="whatsonmenu"><ul><li><a href="/whats-on/search?f[0]=im_vid_45%3A1735&solrsort=is_sticky%20desc%2C%20is_type_grp_0%20asc%2C%20is_end_date%20asc%2C%20is_start_date%20asc%2C%20is_published_date%20desc">Tate Britain</a></li><li><a href="/whats-on/search?f[]=im_vid_45:1762&solrsort=is_sticky%20desc,%20is_type_grp_0%20asc,%20is_end_date%20asc,%20is_start_date%20asc,%20is_published_date%20desc">Tate Modern</a></li><li><a href="/whats-on/search?f[]=im_vid_45:1796&solrsort=is_sticky%20desc,%20is_type_grp_0%20asc,%20is_end_date%20asc,%20is_start_date%20asc,%20is_published_date%20desc">Tate Liverpool</a></li><li><a href="/whats-on/search?f[]=im_vid_45:1808&solrsort=is_sticky%20desc,%20is_type_grp_0%20asc,%20is_end_date%20asc,%20is_start_date%20asc,%20is_published_date%20desc">Tate St Ives</a></li></ul></div>');
    
    // all the facet menu link redirects
    $('.menu-12705').html('<a href="/context-comment/articles/search">Articles</a>');
    $('.menu-12707').html('<a href="/context-comment/blogs/search">Blogs</a>');
    $('.menu-12709').html('<a href="/context-comment/audio-video/search">Channel</a>');
    $('.menu-12711').html('<a href="/context-comment/apps/search">Apps</a>');
    $('.menu-3165').html('<a href="/learn/online-resources/search">Online resources</a>');
    $('.menu-3177').html('<a href="/research/publications/search">Online research publications</a>');
    $('.menu-3154').html('<a href="/about/projects/search">Projects</a>');
    $('.menu-3154').html('<a href="/about/projects/search">Projects</a>');
    
    // caption minimizer
    // $('figcaption p, .lead-image-caption').css('display' , 'none');
    $('figcaption').before('<h3 class="caption-title">Image caption</h3>');
    
    $('.caption-title').each(function() {
      $(this).click(function() {
        if ($(this).hasClass('open')) {
          $(this).next('figcaption').slideUp('fast');
          $(this).removeClass('open')
        }
        else {
          $(this).next('figcaption').slideDown('fast');
          $(this).addClass('open')
        }
      })
    });
    
    $('.lead-image-caption-wrapper').before('<h3 class="lead-img-caption-title">Image caption</h3>');
    
    $('.lead-img-caption-title').each(function() {
      $(this).click(function() {
        if ($(this).hasClass('open')) {
          $(this).next('.lead-image-caption-wrapper').slideUp('fast');
          $(this).removeClass('open')
        }
        else {
          $(this).next('.lead-image-caption-wrapper').slideDown('fast');
          $(this).addClass('open')
        }
      })
    });

    $('.lead-image-caption').before('<h3 class="context-caption-title">Image caption</h3>');

    $('.context-caption-title').each(function() {
      $(this).click(function() {
        if ($(this).hasClass('open')) {
          $(this).next('.lead-image-caption').slideUp('fast');
          $(this).removeClass('open')
        }
        else {
          $(this).next('.lead-image-caption').slideDown('fast');
          $(this).addClass('open')
        }
      })
    });

    var dateNow = new Date();
    var dateString = dateNow.getUTCDate() + '/' + (dateNow.getUTCMonth() + 1) + '/' + dateNow.getUTCFullYear();
    
    $('.menu-mlid-3550').append('<ul class="gallery-submenu"><li><a href="/whats-on/search?f[]=date=' + dateString + '&f[]=im_vid_45:1735&solrsort=is_sticky%20desc,%20is_type_grp_0%20asc,%20is_end_date%20asc,%20is_start_date%20asc,%20is_published_date%20desc">Today at Tate Britain</a></li><li><a href="/visit/tate-britain/admission-opening-times" >Admission and opening times</a></li><li><a href="/visit/tate-britain/getting-here">Getting here</a></li></ul>');
    $('.menu-mlid-737').append('<ul class="gallery-submenu"><li><a href="/whats-on/search?f[]=date=' + dateString + '&f[]=im_vid_45:1762&solrsort=is_sticky%20desc,%20is_type_grp_0%20asc,%20is_end_date%20asc,%20is_start_date%20asc,%20is_published_date%20desc">Today at Tate Modern</a></li><li><a href="/visit/tate-modern/admission-opening-times" >Admission and opening times</a></li><li><a href="/visit/tate-modern/getting-here">Getting here</a></li></ul>');
    $('.menu-mlid-738').append('<ul class="gallery-submenu"><li><a href="/whats-on/search?f[]=date=' + dateString + '&f[]=im_vid_45:1796&solrsort=is_sticky%20desc,%20is_type_grp_0%20asc,%20is_end_date%20asc,%20is_start_date%20asc,%20is_published_date%20desc">Today at Tate Liverpool</a></li><li><a href="/visit/tate-liverpool/admission-and-opening-times" >Admission and opening times</a></li><li><a href="/visit/tate-liverpool/getting-here">Getting here</a></li></ul>');
    $('.menu-mlid-739').append('<ul class="gallery-submenu"><li><a href="/whats-on/search?f[]=date=' + dateString + '&f[]=im_vid_45:1808&solrsort=is_sticky%20desc,%20is_type_grp_0%20asc,%20is_end_date%20asc,%20is_start_date%20asc,%20is_published_date%20desc">Today at Tate St Ives</a></li><li><a href="/visit/tate-st-ives/admission-opening-times" >Admission and opening times</a></li><li><a href="/visit/tate-st-ives/getting-here">Getting here</a></li></ul>');
    $('.facets-current').clone().insertBefore('#search-totals');

    $('.whats-on-tate-britain-today').html('<a href="/whats-on/search?f[]=date=' + dateString + '&f[]=im_vid_45:1735&solrsort=is_sticky%20desc,%20is_type_grp_0%20asc,%20is_end_date%20asc,%20is_start_date%20asc,%20is_published_date%20desc">Today at Tate Britain</a>');
    $('.whats-on-tate-modern-today').html('<a href="/whats-on/search?f[]=date=' + dateString + '&f[]=im_vid_45:1762&solrsort=is_sticky%20desc,%20is_type_grp_0%20asc,%20is_end_date%20asc,%20is_start_date%20asc,%20is_published_date%20desc">Today at Tate Modern</a>');
    $('.whats-on-tate-liverpool-today').html('<a href="/whats-on/search?f[]=date=' + dateString + '&f[]=im_vid_45:1796&solrsort=is_sticky%20desc,%20is_type_grp_0%20asc,%20is_end_date%20asc,%20is_start_date%20asc,%20is_published_date%20desc">Today at Tate Liverpool</a>');
    $('.whats-on-tate-stives-today').html('<a href="/whats-on/search?f[]=date=' + dateString + '&f[]=im_vid_45:1808&solrsort=is_sticky%20desc,%20is_type_grp_0%20asc,%20is_end_date%20asc,%20is_start_date%20asc,%20is_published_date%20desc">Today at Tate St Ives</a>');

    $('#zone-footer h2').click(function() {
      if ($(this).hasClass('open')) {
        $(this).next('div').slideUp('fast');
        $(this).removeClass('open');
      }
      else {
        $(this).next('div').slideDown('fast');
        $(this).addClass('open');
      }
    });

    if ($('body').hasClass('logged-in')) {
      $('.login-handle-title').click(function() {
        if ($(this).hasClass('open')) {
          $(this).next('div').slideUp('fast');
          $(this).removeClass('open');
        }
        else {
          $(this).next('div').slideDown('fast');
          $(this).addClass('open');
        }
      });
    }
    
    /// fix for element-invisible height problem in mobile menu titles since D7.23
    $('.navigation h2').removeClass('element-invisible');

  });
    
  /// added to handle appending destination to links where we have no php power (PG 21 June 2013)
  if ( window.location.search && (String(window.location.search).indexOf('destination=') != -1) ) {
    $(function(){
      var dest = (/(\?|\&)(destination=[^&]+)/i).exec(window.location.search);
      if ( dest && (dest=dest[2]) ) {
        $('a').filter('.js-add-destination').each(function(){
          var link = $(this), href = link.attr('href');
          link.attr('href', href + ( href.indexOf('?') !== -1 ? '&' : '?' ) + dest);
        });
      }
    });
  };
    
})(jQuery);;
