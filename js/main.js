/**
 * @file
 * Main JavaScript file that powers the client-side of Comments v2.
 */
 
(function($){
  
  /*
  /// rearrange behaviours
  (function (){
    var i, s = [];
    /// remove and store all other behaviours, so that we can place our one before AJAX
    for( i in Drupal.behaviors ){
      if ( i == 'AJAX' || s.length ){
        s.push( { key:i, val:Drupal.behaviors[i] } );
        delete Drupal.behaviors[i];
      }
    }
    /// now add in our behaviour
    Drupal.behaviors.tate_comment_and_share = {};
    /// now put the other ones back
    for( i=0; i<s.length;i++ ){
      Drupal.behaviors[s[i].key] = s[i].val;
    }
    var str='';for (i in Drupal.behaviors){str += i+"\n";};alert(str); 
  })();
  */
  
  /// Listen out for events being triggered by Drupal AJAX (or other code)
  /// these have to be bound to an element (rather than document) because
  /// the AJAX invoke method only allows jQuery string selectors.
  /*
  $('body')
    /// Listen out for comment success
    .bind('comment_success.tcas', function( e, context ){
      alert('Comment successful - need to refresh comment listing');
    })
    /// Listen out for comment failure
    .bind('comment_failure.tcas', function( e, context ){
      alert('Comment failed... sorry :(');
    });
  */
  
  Drupal.theme.prototype.tate_comment_and_share_warning = function( message, element ){
    return $('<div class="messages warning tate_comment_and_share_warning">' + message + '</div>');
  }
  
  /// extend our attached behaviour
  Drupal.behaviors.tate_comment_and_share = {
    attach: function (context, settings) {
      var i, item;
      /// step each form that has been identified
      for ( i in settings.tate_comment_and_share ) {
        /// store the item
        item = settings.tate_comment_and_share[i];
        /// remove the item (so it isn't triggered again)
        delete settings.tate_comment_and_share[i];
        /// trigger!
        $(document).trigger("insertform.tcas", item);
      }
    }
  };
  
  /**
   * Communication with this code is via an indirect event listener on the
   * document object, the benefits of this method over using a global 
   * function, are as follows:
   *
   *  1. Other code can listen in for the same event easily
   *  2. Other code can replace or remove this bound function easily
   *  3. Other code does not have direct access to the bound function
   *  4. By keeping access indirect, memory leaks & unwanted references are avoided
   */
  $(document).bind('insertform.tcas', function( e, context ){
    
    /// make sure the DOM is ready
    $(function(){
    
      /// find the target element that we are to replace / modify
      var output = $( '#' + context.output_id ), methods = {}, messages = {}, events = {};
      
      messages.failure = '' +
        '<ul>' + 
        '<li>' + 'Sorry, our comment system seems to be experiencing problems at the moment.' + '</li>' + 
        '<li>' + 'You could <a href="#" class="comment-fallback-link">try commenting here</a> instead.' + '</li>' + 
        '<li>' + '<i>(<b>to prevent loss of your comment</b>, copy it to your clipboard before following the link)</i>' + '</li>' + 
        '</ul>' + 
      '';
      messages.commentEmpty = 'Please comment first before submitting the form';
    
      /// flag this element so that it isn't run multiple times in the same place.
      if( !output.is('.tcas-processed') ){
        output.addClass('.tcas-processed');
      }
      else {
        return;
      }

      /**
       * A submit listener that handles when the comment form should be allowed to submit
       */
      events.submit = function(e){
        /// find the comment field textarea
        var form = $(this), field = output.find('#edit-comment-body-und-0-value'), iid, tid;
        /// check that it has a value
        if ( ! $.trim(field.val()) ) {
          methods.warningMessage( messages.commentEmpty );
          /// stop the submit from working
          return false;
        }
        /// force an error for testing
        if ( field.val() == 'errortest' ) {
          methods.warningMessage( messages.failure );
          methods.setSubmitButton( null );
          methods.setSubmitButton( 'disabled', '' );
          return false;
        }
        /// disable the submit button (to stop dual submits)
        methods.setSubmitButton('Please wait...');
        methods.setSubmitButton('disabled', 'disabled');
        /// it is possible the user could submit before our AJAX values have
        /// been injected to the form. If so we should stall for time.
        if ( output.is('.disabled-form') ) {
          /// keep watch to see if the form becomes un-disabled
          iid = setInterval(function(){
            if ( !output.is('.disabled-form') ) {
              /// stop our timeouts
              clearTimeout(tid);
              clearInterval(iid);
              /// re-enable the submit, just in case
              methods.setSubmitButton('disabled', '');
              /// if our form becomes un-disabled, then submit
              form.submit();
            }
          },100);
          /// don't keep watch for too long however... if after 5 seconds
          /// the form is still disabled, we should stop and error to the user
          tid = setTimeout(function(){
            methods.warningMessage( messages.failure );
            methods.setSubmitButton( null );
            methods.setSubmitButton( 'disabled', '' );
            clearInterval(iid);
          },10000);
          return false;
        }
        /// disable the cancel reply button
        $('.comment-cancel-reply')
          .attr('disabled', 'disabled');
      }

      /**
       * Sets a warning message similar to drupal_set_message only client-side
       *
       * @param string - the string message
       */ 
      methods.warningMessage = function( message ){
        /// remove existing error messages
        $('.tate_comment_and_share_warning').remove();
        if ( message ) {
          /// generate the error message
          message = Drupal.theme('tate_comment_and_share_warning', message);
          /// check for the comment fallback link
          if ( message.find('.comment-fallback-link').length ) {
            /// if it exists, update it's href to that of the form's action
            message.find('.comment-fallback-link').attr('href', output.find('form').attr('action') );
          }
          /// insert error message
          output.prepend( message.hide().fadeIn() );
        }
      }
    
      /**
       * Function used to tailor the comment for to be a reply form for a particular comment
       *
       * @param element - the reply comment-reply LI element that contains the reply link
       */
      methods.replyToComment = function( replyLink ){
        if ( replyLink ) {
          var link = $(replyLink),
              comment = link.closest('.comment'),
              href = link.attr('href'),
              pid = String(href).split('/').pop();
          /// show the cancel reply button
          $('.comment-cancel-reply')
            .show();
          output.slideUp(function(){
            /// clear any warning message
            methods.warningMessage();
            /// shift our comment form to the right place for a reply
            comment.append( output );
            /// renable the reply form with this comments id as a pid
            methods.enableForm( pid );
            /// change the comment form's destination action
            output.find('form').attr('action', href );
            /// make the form appear as if it is already a reply using css indenting
            output.removeClass('indented').addClass('indented');
            /// change the submit button to enforce the 'reply' route
            methods.setSubmitButton( 'Reply' );
            /// change the title to mention replying
            output.find('#comment-form-title').html('Reply to comment');
            /// reveal the comment form
            output.slideDown();
          });
        }
        else {
          /// hide the cancel reply button
          $('.comment-cancel-reply')
            .hide();
          output.slideUp(function(){
            /// clear any warning message
            methods.warningMessage();
            /// stop the reply / indent style
            output.removeClass('indented');
            /// retrieve the original submit text
            methods.setSubmitButton( null );
            /// insert the comment form back where it first was
            $('#comments').after( output );
            /// renable the reply form without a pid
            methods.enableForm();
            /// change the comment form's destination action back to it's original
            output.find('form').attr('action', output.data('tcas-action') );
            /// set the title to the original title
            output.find('#comment-form-title').html( output.data('tcas-original-title') );
            /// reveal the comment form
            output.slideDown();
          });
        }
        return false;
      }
    
      /**
       * Simple function that takes our disabled form, asks Drupal
       * for the right values, and then using those values, enables it.
       */
      methods.enableForm = function( pid ){
        /// this function can be called by both event listeners and as a normal function call
        if( typeof pid == 'object' ){
          /// if we receive an object instead of a pid, just blank as it will be an event object
          pid = '';
        }
        /// pull out the form state
        var state = output.data('tcas_form_state') || {pid:-1};
        /// build the request URL that will provide us with fresh form values
        var url = '/api/comments/form/' + context.nid+ ( pid ? '/' + pid : '' );
        /// quick help with testing, this will break the url is #errortest is present in the url
        if ( window.location && window.location.hash && ( window.location.hash == '#errortest' || window.location.hash == 'errortest' ) ) {
          url = '/errortest/' + url;
        }
        /// if the state is the same, no point in requesting a new form
        if ( state.pid != pid ) {
          /// disable the form whilst we are enabling
          output.removeClass('disabled-form').addClass('disabled-form');
          /// update the state to reflect the new request
          state.pid = pid;
          /// store the new form state
          output.data('tcas_form_state', state);
          /// make sure this function can't be executed again
          output
            .find('textarea, input, select, button')
              .unbind('focus', methods.enableForm);
              //.unbind('mouseover', methods.enableForm);
          /// get the fresh values from drupal
          $.ajax(url, {
            success: function( resp, state ){
              /// were we successful? and do we have the minimum data?
              if ( resp && resp['form_build_id'] ) {
                /// step each item returned and populate the form's hidden fields
                for( var i in resp ){
                  output.find('[name='+i+']').val( resp[i] );
                }
                /// attempt to attach drupal's ajax stuff
                Drupal.attachBehaviors( output );
                /// remove the disabled class
                output.removeClass('disabled-form');
              }
            },
            error: function( xhttp, state ){
              if ( state == 'abort' || state == 'success' || methods.checkXHRForUserAbort(xhttp) ) {
                /// don't warn the user if they interrupt our quiet process
              }
              else {
                /// Warn the user about the failed ajax attempt
                methods.warningMessage( messages.failure );
              }
              /// remove the disabled class
              output.removeClass('disabled-form');
            }
          });
        }
      }

      /**
       * Simple function to handle setting the value (or other attributes) of the
       * comment submit button.
       *
       * @param text - a string representing the text value of the button to use
       * @param attrValue (optional) - if this param is present the text param gets 
       *   treated as attrName and the function pretty much behaves as $().attr.
       */
      methods.setSubmitButton = function( text, attrValue ){
        if ( arguments.length == 2 ) {
          if ( !attrValue ) {
            output.find('.form-submit').removeAttr(text);
          }
          else {
            output.find('.form-submit').attr(text, attrValue);
          }
        }
        else {
          if ( text ) {
            output.find('.form-submit').val( text );
          }
          else {
            /// retrieve the original submit text
            output.find('.form-submit').val( output.find('.form-submit').data('previous-value') );
          }
        }
      }

      /**
       * Simple function that is used to embed the disabled form into the DOM
       * Javascript then manages on when to enable the form using methods.enableForm()
       *
       * Currently the form will be enabled under the following events:
       *
       *  1. When a user focuses on an element within the comment form
       *  2. When a user clicks a reply link on any of the comments
       *  3. When a user clicks the "Cancel Reply" button.
       *
       * Enabling the form involves XHTTP communication with the backend and getting
       * fresh form tokens and ids. Once these ids have been injected to the form DOM,
       * the form is then live and can be submitted.
       *
       * The reasons for injecting these values quietly are:
       *
       *  1. We wanted to start with a form actually in the page, so that users can comment more easily. However,
       *       any form that is written directly into the page will become cached, and will be disabled by that caching.
       *       Due to hidden security values in the form will become out-of-date or invalid after one use.
       *  2. By requesting and then inserting fresh security values quielty (rather than HTML rewritting 
       *       the entire form) we can allow the user to start using the form whilst we are making the XHTTP request 
       *       to the backend and enabling the form.
       */
      methods.embedForm = function(){
        /// keep the form html actually stored within the cached page
        if ( output && output.length && context.form_html ) {
          /// if we find our output div and we have form html to write, then do so
          output
            .html(context.form_html);
          /// store the original form path, incase we modify it later
          output
            .data('tcas-action', output.find('form').attr('action') );
          output
            .data('tcas-original-title', output.find('#comment-form-title').text());
          /// prep the dom elements
          output
            /// initially flag the form as disabled
            .addClass('disabled-form')
            /// then find all inputs within the form
            .find('textarea, input, select')
              /// and bind to the focus and mouseover events the "enable form" code
              .bind('focus', methods.enableForm);
              //.bind('mouseover', methods.enableForm);
          output
            .find('.form-submit')
              .data('previous-value', output.find('.form-submit').val() )
              .after( 
                $('<input class="comment-cancel-reply" title="Click to convert your comment from a reply, to that of a standard comment." type="button" value="Cancel Reply" />')
                  .click(function(){methods.replyToComment( null );})
                  .hide()
              );
          output
            .find('form')
              .submit( events.submit );
          /// find all comment reply links, and take over their click
          $('.comment-reply a').live('click', function(){
            return methods.replyToComment( this );
          });
          /// overwrite the comment add links so they stay on this page and not use
          /// the non-js fallback.
          $('.comment-add a').attr('href', '#comment-form-title');
        }else if ( output && output.length && !context.form_html ) {
          /// if no html, it is likely our comment form has already been rendered on the page
          /// in which case, we should remove our fallback URL.
          output.remove();
        }
      }
      
      methods.checkXHRForUserAbort = function(xhr){
        if ( xhr && xhr.getAllResponseHeaders ) {
          return !xhr.getAllResponseHeaders();
        }
        else {
          return null;
        }
      }
    
      /// start the indiana-polystyrene-ball rolling
      methods.embedForm();
      
    });
    
  });
  
})(window && window.jQuery);;
;(function($){
//  /// listen out for any cacheless messages to do with tate_comment_and_share
//  Drupal.behaviors.tate_comment_and_share__messages = {
//    attach: function(context, settings) {
//      var
//        msgs = settings.tate_comment_and_share__messages,
//        data = String($.cookie('tate.cacheless.messages')).split(',')
//      ;
//      if ( data.length ) {
//        var gui = {
//          zonePreface: $('#zone-preface')
//        };
//        /// create the normal zone preface (if it doesn't exists) where we currently place our messages.
//        if ( !gui.zonePreface.length ) {
//          /// add the bare minimun of identification, it's hacky enought to have to replicate grid dom!
//          gui.zonePreface = $('<div id="zone-preface" />')
//            .append('<div id="region-preface-first" class="region region-preface-first" />')
//            /// To avoid having to work out what grid this region is using, cheat and insert it
//            /// with no griding inside the content zone.
//            .prependTo('#zone-content')
//          ;
//        }
//        /// append our messages
//        for ( var i=0; i<data.length; i++ ) {
//          if ( msgs[data[i]] ) {
//            /// create each message and append to the messageregion
//            (message = Drupal.theme('tate_comment_and_share_warning', msgs[data[i]]))
//              .appendTo(gui.zonePreface.find('.region-preface-first'))
//            ;
//          }
//        }
//      }
//      /// remove the cookie, once the messages have been displayed
//      $.cookie('tate.cacheless.messages', '', {path: '/'});
//    }
//  };

})( window.jQuery && jQuery );;
/*!
	Colorbox v1.4.26 - 2013-06-30
	jQuery lightbox and modal window plugin
	(c) 2013 Jack Moore - http://www.jacklmoore.com/colorbox
	license: http://www.opensource.org/licenses/mit-license.php
*/
(function(e,t,i){function o(i,o,n){var r=t.createElement(i);return o&&(r.id=et+o),n&&(r.style.cssText=n),e(r)}function n(){return i.innerHeight?i.innerHeight:e(i).height()}function r(e){var t=E.length,i=(j+e)%t;return 0>i?t+i:i}function l(e,t){return Math.round((/%/.test(e)?("x"===t?H.width():n())/100:1)*parseInt(e,10))}function a(e,t){return e.photo||e.photoRegex.test(t)}function h(e,t){return e.retinaUrl&&i.devicePixelRatio>1?t.replace(e.photoRegex,e.retinaSuffix):t}function s(e){"contains"in v[0]&&!v[0].contains(e.target)&&(e.stopPropagation(),v.focus())}function d(){var t,i=e.data(A,Z);null==i?(O=e.extend({},Y),console&&console.log&&console.log("Error: cboxElement missing settings object")):O=e.extend({},i);for(t in O)e.isFunction(O[t])&&"on"!==t.slice(0,2)&&(O[t]=O[t].call(A));O.rel=O.rel||A.rel||e(A).data("rel")||"nofollow",O.href=O.href||e(A).attr("href"),O.title=O.title||A.title,"string"==typeof O.href&&(O.href=e.trim(O.href))}function c(i,o){e(t).trigger(i),ht.trigger(i),e.isFunction(o)&&o.call(A)}function u(){var e,t,i,o,n,r=et+"Slideshow_",l="click."+et;O.slideshow&&E[1]?(t=function(){clearTimeout(e)},i=function(){(O.loop||E[j+1])&&(e=setTimeout(J.next,O.slideshowSpeed))},o=function(){R.html(O.slideshowStop).unbind(l).one(l,n),ht.bind(nt,i).bind(ot,t).bind(rt,n),v.removeClass(r+"off").addClass(r+"on")},n=function(){t(),ht.unbind(nt,i).unbind(ot,t).unbind(rt,n),R.html(O.slideshowStart).unbind(l).one(l,function(){J.next(),o()}),v.removeClass(r+"on").addClass(r+"off")},O.slideshowAuto?o():n()):v.removeClass(r+"off "+r+"on")}function p(i){G||(A=i,d(),E=e(A),j=0,"nofollow"!==O.rel&&(E=e("."+tt).filter(function(){var t,i=e.data(this,Z);return i&&(t=e(this).data("rel")||i.rel||this.rel),t===O.rel}),j=E.index(A),-1===j&&(E=E.add(A),j=E.length-1)),g.css({opacity:parseFloat(O.opacity),cursor:O.overlayClose?"pointer":"auto",visibility:"visible"}).show(),V&&v.add(g).removeClass(V),O.className&&v.add(g).addClass(O.className),V=O.className,O.closeButton?P.html(O.close).appendTo(x):P.appendTo("<div/>"),$||($=q=!0,v.css({visibility:"hidden",display:"block"}),W=o(st,"LoadedContent","width:0; height:0; overflow:hidden").appendTo(x),_=b.height()+k.height()+x.outerHeight(!0)-x.height(),D=T.width()+C.width()+x.outerWidth(!0)-x.width(),N=W.outerHeight(!0),z=W.outerWidth(!0),O.w=l(O.initialWidth,"x"),O.h=l(O.initialHeight,"y"),J.position(),u(),c(it,O.onOpen),B.add(S).hide(),v.focus(),O.trapFocus&&t.addEventListener&&(t.addEventListener("focus",s,!0),ht.one(lt,function(){t.removeEventListener("focus",s,!0)})),O.returnFocus&&ht.one(lt,function(){e(A).focus()})),w())}function f(){!v&&t.body&&(X=!1,H=e(i),v=o(st).attr({id:Z,"class":e.support.opacity===!1?et+"IE":"",role:"dialog",tabindex:"-1"}).hide(),g=o(st,"Overlay").hide(),L=e([o(st,"LoadingOverlay")[0],o(st,"LoadingGraphic")[0]]),y=o(st,"Wrapper"),x=o(st,"Content").append(S=o(st,"Title"),M=o(st,"Current"),K=e('<button type="button"/>').attr({id:et+"Previous"}),I=e('<button type="button"/>').attr({id:et+"Next"}),R=o("button","Slideshow"),L),P=e('<button type="button"/>').attr({id:et+"Close"}),y.append(o(st).append(o(st,"TopLeft"),b=o(st,"TopCenter"),o(st,"TopRight")),o(st,!1,"clear:left").append(T=o(st,"MiddleLeft"),x,C=o(st,"MiddleRight")),o(st,!1,"clear:left").append(o(st,"BottomLeft"),k=o(st,"BottomCenter"),o(st,"BottomRight"))).find("div div").css({"float":"left"}),F=o(st,!1,"position:absolute; width:9999px; visibility:hidden; display:none"),B=I.add(K).add(M).add(R),e(t.body).append(g,v.append(y,F)))}function m(){function i(e){e.which>1||e.shiftKey||e.altKey||e.metaKey||e.ctrlKey||(e.preventDefault(),p(this))}return v?(X||(X=!0,I.click(function(){J.next()}),K.click(function(){J.prev()}),P.click(function(){J.close()}),g.click(function(){O.overlayClose&&J.close()}),e(t).bind("keydown."+et,function(e){var t=e.keyCode;$&&O.escKey&&27===t&&(e.preventDefault(),J.close()),$&&O.arrowKey&&E[1]&&!e.altKey&&(37===t?(e.preventDefault(),K.click()):39===t&&(e.preventDefault(),I.click()))}),e.isFunction(e.fn.on)?e(t).on("click."+et,"."+tt,i):e("."+tt).live("click."+et,i)),!0):!1}function w(){var n,r,s,u=J.prep,p=++dt;q=!0,U=!1,A=E[j],d(),c(at),c(ot,O.onLoad),O.h=O.height?l(O.height,"y")-N-_:O.innerHeight&&l(O.innerHeight,"y"),O.w=O.width?l(O.width,"x")-z-D:O.innerWidth&&l(O.innerWidth,"x"),O.mw=O.w,O.mh=O.h,O.maxWidth&&(O.mw=l(O.maxWidth,"x")-z-D,O.mw=O.w&&O.w<O.mw?O.w:O.mw),O.maxHeight&&(O.mh=l(O.maxHeight,"y")-N-_,O.mh=O.h&&O.h<O.mh?O.h:O.mh),n=O.href,Q=setTimeout(function(){L.show()},100),O.inline?(s=o(st).hide().insertBefore(e(n)[0]),ht.one(at,function(){s.replaceWith(W.children())}),u(e(n))):O.iframe?u(" "):O.html?u(O.html):a(O,n)?(n=h(O,n),U=t.createElement("img"),e(U).addClass(et+"Photo").bind("error",function(){O.title=!1,u(o(st,"Error").html(O.imgError))}).one("load",function(){var t;p===dt&&(U.alt=e(A).attr("alt")||e(A).attr("data-alt")||"",O.retinaImage&&i.devicePixelRatio>1&&(U.height=U.height/i.devicePixelRatio,U.width=U.width/i.devicePixelRatio),O.scalePhotos&&(r=function(){U.height-=U.height*t,U.width-=U.width*t},O.mw&&U.width>O.mw&&(t=(U.width-O.mw)/U.width,r()),O.mh&&U.height>O.mh&&(t=(U.height-O.mh)/U.height,r())),O.h&&(U.style.marginTop=Math.max(O.mh-U.height,0)/2+"px"),E[1]&&(O.loop||E[j+1])&&(U.style.cursor="pointer",U.onclick=function(){J.next()}),U.style.width=U.width+"px",U.style.height=U.height+"px",setTimeout(function(){u(U)},1))}),setTimeout(function(){U.src=n},1)):n&&F.load(n,O.data,function(t,i){p===dt&&u("error"===i?o(st,"Error").html(O.xhrError):e(this).contents())})}var g,v,y,x,b,T,C,k,E,H,W,F,L,S,M,R,I,K,P,B,O,_,D,N,z,A,j,U,$,q,G,Q,J,V,X,Y={transition:"elastic",speed:300,fadeOut:300,width:!1,initialWidth:"600",innerWidth:!1,maxWidth:!1,height:!1,initialHeight:"450",innerHeight:!1,maxHeight:!1,scalePhotos:!0,scrolling:!0,inline:!1,html:!1,iframe:!1,fastIframe:!0,photo:!1,href:!1,title:!1,rel:!1,opacity:.9,preloading:!0,className:!1,retinaImage:!1,retinaUrl:!1,retinaSuffix:"@2x.$1",current:"image {current} of {total}",previous:"previous",next:"next",close:"close",xhrError:"This content failed to load.",imgError:"This image failed to load.",open:!1,returnFocus:!0,trapFocus:!0,reposition:!0,loop:!0,slideshow:!1,slideshowAuto:!0,slideshowSpeed:2500,slideshowStart:"start slideshow",slideshowStop:"stop slideshow",photoRegex:/\.(gif|png|jp(e|g|eg)|bmp|ico|webp)((#|\?).*)?$/i,onOpen:!1,onLoad:!1,onComplete:!1,onCleanup:!1,onClosed:!1,overlayClose:!0,escKey:!0,arrowKey:!0,top:!1,bottom:!1,left:!1,right:!1,fixed:!1,data:void 0,closeButton:!0},Z="colorbox",et="cbox",tt=et+"Element",it=et+"_open",ot=et+"_load",nt=et+"_complete",rt=et+"_cleanup",lt=et+"_closed",at=et+"_purge",ht=e("<a/>"),st="div",dt=0,ct={};e.colorbox||(e(f),J=e.fn[Z]=e[Z]=function(t,i){var o=this;if(t=t||{},f(),m()){if(e.isFunction(o))o=e("<a/>"),t.open=!0;else if(!o[0])return o;i&&(t.onComplete=i),o.each(function(){e.data(this,Z,e.extend({},e.data(this,Z)||Y,t))}).addClass(tt),(e.isFunction(t.open)&&t.open.call(o)||t.open)&&p(o[0])}return o},J.position=function(t,i){function o(){b[0].style.width=k[0].style.width=x[0].style.width=parseInt(v[0].style.width,10)-D+"px",x[0].style.height=T[0].style.height=C[0].style.height=parseInt(v[0].style.height,10)-_+"px"}var r,a,h,s=0,d=0,c=v.offset();if(H.unbind("resize."+et),v.css({top:-9e4,left:-9e4}),a=H.scrollTop(),h=H.scrollLeft(),O.fixed?(c.top-=a,c.left-=h,v.css({position:"fixed"})):(s=a,d=h,v.css({position:"absolute"})),d+=O.right!==!1?Math.max(H.width()-O.w-z-D-l(O.right,"x"),0):O.left!==!1?l(O.left,"x"):Math.round(Math.max(H.width()-O.w-z-D,0)/2),s+=O.bottom!==!1?Math.max(n()-O.h-N-_-l(O.bottom,"y"),0):O.top!==!1?l(O.top,"y"):Math.round(Math.max(n()-O.h-N-_,0)/2),v.css({top:c.top,left:c.left,visibility:"visible"}),y[0].style.width=y[0].style.height="9999px",r={width:O.w+z+D,height:O.h+N+_,top:s,left:d},t){var u=0;e.each(r,function(e){return r[e]!==ct[e]?(u=t,void 0):void 0}),t=u}ct=r,t||v.css(r),v.dequeue().animate(r,{duration:t||0,complete:function(){o(),q=!1,y[0].style.width=O.w+z+D+"px",y[0].style.height=O.h+N+_+"px",O.reposition&&setTimeout(function(){H.bind("resize."+et,J.position)},1),i&&i()},step:o})},J.resize=function(e){var t;$&&(e=e||{},e.width&&(O.w=l(e.width,"x")-z-D),e.innerWidth&&(O.w=l(e.innerWidth,"x")),W.css({width:O.w}),e.height&&(O.h=l(e.height,"y")-N-_),e.innerHeight&&(O.h=l(e.innerHeight,"y")),e.innerHeight||e.height||(t=W.scrollTop(),W.css({height:"auto"}),O.h=W.height()),W.css({height:O.h}),t&&W.scrollTop(t),J.position("none"===O.transition?0:O.speed))},J.prep=function(i){function n(){return O.w=O.w||W.width(),O.w=O.mw&&O.mw<O.w?O.mw:O.w,O.w}function l(){return O.h=O.h||W.height(),O.h=O.mh&&O.mh<O.h?O.mh:O.h,O.h}if($){var s,d="none"===O.transition?0:O.speed;W.empty().remove(),W=o(st,"LoadedContent").append(i),W.hide().appendTo(F.show()).css({width:n(),overflow:O.scrolling?"auto":"hidden"}).css({height:l()}).prependTo(x),F.hide(),e(U).css({"float":"none"}),s=function(){function i(){e.support.opacity===!1&&v[0].style.removeAttribute("filter")}var n,l,s=E.length,u="frameBorder",p="allowTransparency";$&&(l=function(){clearTimeout(Q),L.hide(),c(nt,O.onComplete)},S.html(O.title).add(W).show(),s>1?("string"==typeof O.current&&M.html(O.current.replace("{current}",j+1).replace("{total}",s)).show(),I[O.loop||s-1>j?"show":"hide"]().html(O.next),K[O.loop||j?"show":"hide"]().html(O.previous),O.slideshow&&R.show(),O.preloading&&e.each([r(-1),r(1)],function(){var i,o,n=E[this],r=e.data(n,Z);r&&r.href?(i=r.href,e.isFunction(i)&&(i=i.call(n))):i=e(n).attr("href"),i&&a(r,i)&&(i=h(r,i),o=t.createElement("img"),o.src=i)})):B.hide(),O.iframe?(n=o("iframe")[0],u in n&&(n[u]=0),p in n&&(n[p]="true"),O.scrolling||(n.scrolling="no"),e(n).attr({src:O.href,name:(new Date).getTime(),"class":et+"Iframe",allowFullScreen:!0,webkitAllowFullScreen:!0,mozallowfullscreen:!0}).one("load",l).appendTo(W),ht.one(at,function(){n.src="//about:blank"}),O.fastIframe&&e(n).trigger("load")):l(),"fade"===O.transition?v.fadeTo(d,1,i):i())},"fade"===O.transition?v.fadeTo(d,0,function(){J.position(0,s)}):J.position(d,s)}},J.next=function(){!q&&E[1]&&(O.loop||E[j+1])&&(j=r(1),p(E[j]))},J.prev=function(){!q&&E[1]&&(O.loop||j)&&(j=r(-1),p(E[j]))},J.close=function(){$&&!G&&(G=!0,$=!1,c(rt,O.onCleanup),H.unbind("."+et),g.fadeTo(O.fadeOut||0,0),v.stop().fadeTo(O.fadeOut||0,0,function(){v.add(g).css({opacity:1,cursor:"auto"}).hide(),c(at),W.empty().remove(),setTimeout(function(){G=!1,c(lt,O.onClosed)},1)}))},J.remove=function(){v&&(v.stop(),e.colorbox.close(),v.stop().remove(),g.remove(),G=!1,v=null,e("."+tt).removeData(Z).removeClass(tt),e(t).unbind("click."+et))},J.element=function(){return e(A)},J.settings=Y)})(jQuery,document,window);;
(function ($) {

Drupal.behaviors.initColorbox = {
  attach: function (context, settings) {
    if (!$.isFunction($.colorbox)) {
      return;
    }
    $('.colorbox', context)
      .once('init-colorbox')
      .colorbox(settings.colorbox);
  }
};

{
  $(document).bind('cbox_complete', function () {
    Drupal.attachBehaviors('#cboxLoadedContent');
  });
}

})(jQuery);
;
(function ($) {

Drupal.behaviors.initColorboxLoad = {
  attach: function (context, settings) {
    if (!$.isFunction($.colorbox)) {
      return;
    }
    $.urlParams = function (url) {
      var p = {},
          e,
          a = /\+/g,  // Regex for replacing addition symbol with a space
          r = /([^&=]+)=?([^&]*)/g,
          d = function (s) { return decodeURIComponent(s.replace(a, ' ')); },
          q = url.split('?');
      while (e = r.exec(q[1])) {
        e[1] = d(e[1]);
        e[2] = d(e[2]);
        switch (e[2].toLowerCase()) {
          case 'true':
          case 'yes':
            e[2] = true;
            break;
          case 'false':
          case 'no':
            e[2] = false;
            break;
        }
        if (e[1] == 'width') { e[1] = 'innerWidth'; }
        if (e[1] == 'height') { e[1] = 'innerHeight'; }
        p[e[1]] = e[2];
      }
      return p;
    };
    $('.colorbox-load', context)
      .once('init-colorbox-load', function () {
        var params = $.urlParams($(this).attr('href'));
        $(this).colorbox($.extend({}, settings.colorbox, params));
      });
  }
};

})(jQuery);
;
// $Id: hint.js,v 1.4 2009/12/20 02:05:12 quicksketch Exp $
(function ($) {

/**
 * The Drupal behavior to add the $.hint() behavior to elements.
 */
Drupal.behaviors.hint = {};
Drupal.behaviors.hint.attach = function(content) {
  // Even though it's unlikely that another class name would be used, we ensure
  // that the behavior uses the default class names used in the module.
  jQuery('input.hint-enabled:not(input.hint)', content).hint({
    hintClass: 'hint',
    triggerClass: 'hint-enabled'
  });
};

/**
 * The jQuery method $.hint().
 *
 * This method can be used on any text field or password element and is not
 * Drupal-specific. Any elements using hint must have a "title" attribute,
 * which will be used as the hint.
 */
jQuery.fn.hint = function(options) {
  var opts = jQuery.extend(false, jQuery.fn.hint.defaults, options);

  $(this).find('input').andSelf().filter('[type=text], [type=password]').each(function() {
    if (this.title) {
      var attributes = opts.keepAttributes;
      var $element = $(this);
      var $placeholder = $('<input type="textfield" value="" />');

      // Set the attributes on our placeholder.
      for (var key in attributes) {
        var attribute = attributes[key];
        $placeholder.attr(attribute, $element.get(0)[attribute]);
      }
      $placeholder.val($element.get(0).title);
      $placeholder.get(0).autocomplete = false;
      $placeholder.removeClass(opts.triggerClass).addClass(opts.hintClass);
      $element.after($placeholder);
      if ($element.val() == '') {
        $element.hide();
      }
      else {
        $placeholder.hide();
      }

      $placeholder.focus(function() {
        $placeholder.hide();
        $element.show().get(0).focus();
      });
      $element.blur(function() {
        if (this.value == '') {
          $element.hide();
          $placeholder.show();
        }
      });
    }
  });
};

jQuery.fn.hint.defaults = {
  // The class given the textfield containing the hint.
  hintClass: 'hint',

  // A class that will trigger the hint if $.hint() is used on multiple
  // elements or the entire page.
  triggerClass: 'hint-enabled',

  // A list of attributes that will be copied to the placeholder element.
  // Usually this list will be sufficient, but a special list may be specified
  // if needing to keep custom or obscure attributes.
  keepAttributes: ['style', 'className', 'title', 'size']
};

})(jQuery);
;
(function($){
  
  var autofunc = {
    /// searched for a title within the clicked element
    Title: function( item, data ){
      return $.trim($(item).find('.column_title,h1,h2,h3,h4,h5').eq(0).text());
    },
    NearestTitle: function( item, data ){
      /// try and find a near title
      var i = 4, p = $(item);
      while ( p.length && --i ) {
        if ( p.find('h1,h2,h3,h4,h5').length ) {
          p = p.find('h1,h2,h3,h4,h5'); break;
        }
        p = p.parent();
      }
      /// we found a title!
      if( i && p.length ){
        return $.trim(String(p.eq(0).text()));
      }else{
        return 'Unknown';
      }
    },
    Value: function( item, data ){
      return $.trim(String($(item).val()));
    },
    Text: function( item, data ){
      return $.trim(String($(item).text()));
    },
    HTML: function( item, data ){
      return $.trim(String($(item).html()));
    },
    PageURL: function( item, data ){
      return String(window.location);
    },
    Link: function( item, data ){
      item = $(item);
      return String(item.attr('href')).toLowerCase();
    },
    LinkClean: function( item, data ){
      item = $(item);
      return String(item.attr('href'))
        .toLowerCase()
        .replace('http://','')
        .replace('https://','')
        .replace('mailto://','')
        .replace('www.','');
    },
    LinkExtension: function( item, data ){
      item = $(item);
      var parts, href = String(item.attr('href')).toLowerCase();
      parts = href.split('?');
      parts = parts[0].split('/');
      parts = parts[parts.length-1].split('.');
      parts = parts[parts.length-1];
      return parts;
    },
    LinkFileType: function( item, data ){
      item = $(item);
      /// handle the already rewritten in-body download links
      if( item.attr('data-download-type') ){
        return item.attr('data-download-type');
      }else{ /// handle the field generated download links
        var i, html = '';
        /// if the item's parent contains other links, we can't rely on converting it's content to text
        if( item.parent().find('a[href]').length > 1 ){
          /// instead lets try looking ahead for a span, which may contain our file type.
          html = String(item.next('span').text()).toLowerCase();
        }
        else{
          /// if however, our parent only contains one link, we can safely scan the whole text.
          html = String(item.parent().text()).toLowerCase();
        }
        /// scan the html for file types
        if ( html ) {
          for( i in data.icons ){
            if( html.indexOf(i) != -1 ){
              return i;
            }
          }
        }
        return 'unknown';
      }
    }
  }
  
  /// create our closure to trap the item var on each iteration
  var createHandler = function( item, data ){
    return function(e){
      var category, action, label, f, target = $(this), a, t;
      /// if we are dealing with a click event on a link, then prevent the normal click
      if( (item.interaction == 'click') && target.is('a') ){
        e.preventDefault();
      }
      /// pull out the items so we don't trash the originals
      category = item.category;
      action = item.action;
      label = item.title;
      /// try to automate category values
      if( !category && item.category_value && (f=autofunc[item.category_value]) ){
        category = f(this, data);
      }
      /// try to automate action values
      if( !action && item.action_value && (f=autofunc[item.action_value]) ){
        action = f(this, data);
      }
      /// try to automate label values
      if( !label && item.title_value && (f=autofunc[item.title_value]) ){
        label = f(this, data);
      }      
      /// if we have everything we need, then track...
      try{
        a = [ '_trackEvent', category, action, label ];
        t = false;
        if( category && action && label ){
          if( window && window._gaq ){
            _gaq.push(a);
            t = true;
            if( data.sga && window && window.console ){
              window.console.log('Tracked '+a);
            }
          }
        }
      }catch(ee){}
      /// if we failed to track.. log
      if( !t ){
        if( data.sga && window && window.console ){
          window.console.log('Failed to track '+a);
        }
      }
      
      if( data.sga ){ 
        /// disable links when in show track mode
      }else if( (item.interaction == 'click') && target.is('a') ){
        /// if we are dealing with a click event on a link, trigger the link after a delay
        setTimeout(function(){
          window.location = String(target.attr('href'));
        },100);
      }
    };
  }
  
  Drupal.behaviors.tateMisc = {
    attach: function(context, settings) {
      var i, data, s, item;
      var bad = new RegExp('(^|[^a-z0-9_\\-])(window|document|this|eval|history)([^a-z0-9_\\-]|$)','gi');
      /// -------------------------------------------------------------------------
      /// TRACK ADMIN CONTROLLED LINKS
      /// -------------------------------------------------------------------------
      /// have removed this fallback as it prevents the possibility of this code
      /// being compromised by global modification of Drupal.settings;
      //if (!settings){ settings = Drupal.settings; }
      
      /// see tate misc module for these settings
      if( (data = settings.tateMisc) ){
        /// each item should represet a jQuery selector
        for( i in data.items ){
          /// pull out the item & check that it does indeed have a selector
          if( (item = data.items[i]) && item.selector ){
            /// Safety checking - my check above should block major items that can be
            /// used for nefarious means, bar modifying the HTML on the current page.
            /// but then if someone were to gain access to be able to change Drupal's settings
            /// array they'd be able to do more than just rewrite HTML.
            if( bad && item.selector.match && item.selector.match(bad) ){
              /// skip out selectors that contain object references to prevent against
              /// evaling dodgy selectors.
              continue;
            }
            /// execute the selector with jQuery
            try{s=null;eval('s='+item.selector);}catch(ee){alert(ee);s=null;}
            /// if we got something back then continue.
            if( (s instanceof jQuery) && s.length ){
              /// @todo - remove before live
              s.addClass('ga-tracked');
              if ( data.sga ) {
                s.addClass('ga-highlight');
              }
              /// check what kind of interaction has been requested
              switch( item.interaction ){
                case 'click':
                case 'mouseover':
                case 'focus':
                case 'blur':
                  /// these events would work best as live handlers
                  s.live(item.interaction, createHandler(item, data));
                break;
                case 'submit':
                  /// submit can't be live
                  s.bind(item.interaction, createHandler(item, data));
                break;
              }
            }
          }
        }
        /// -------------------------------------------------------------------------
        /// IMPROVE GUI
        /// -------------------------------------------------------------------------
        /// auto handle icons for download links in body content
        /// -------------------------------------------------------------------------
        $('#region-content li > a').filter('[href*=\'download/\'], [href*=\'file/\']').each(function(){
          var item = $(this), type = 'file',
              /// create a clone of the li, remove the link, so we can get to the text
              clone = item.parent().clone().find('a').remove().end(),
              /// I've limited the substr to 300 chars, although this could cause
              /// problems if the Editors ever start creating "download type"
              /// descriptions that are longer. The limitation is there because
              /// download links could be incorrectly placed as part of a huge body 
              /// of text, which we don't want to spend time processing.
              text = $.trim(clone.text().substring(0,300).toLowerCase());
          /// if there is text to parse to guess at what file type, then do so
          if( text ){
            for( i in data.icons ){
              if( text.indexOf(i) != -1 ){
                type = i;
                break;
              }
            }
          }
          /// insert the correct icon, if it exists, default to file if not
          if( type && data.icons[type] ){
            item.addClass('in-body-download');
            item.attr('data-download-type', type);
            item.before('<img src="/'+data.icons[type]+'" class="in-body-download-icon" />');
          }
        });
      }
      /// -------------------------------------------------------------------------
      /// TRACK SOCIAL ITEMS
      /// -------------------------------------------------------------------------
      /// track twitter
      /// -------------------------------------------------------------------------
      if( $('#sharetwitter').length ){
        try{
          twttr.events.bind('tweet', function(event) {
            if (event) {
              var targetUrl;
              if (event.target && String(event.target.nodeName).toLowerCase() == 'iframe') {
                targetUrl = extractParamFromUri(event.target.src, 'url');
              }
              _gaq.push(['_trackSocial', 'twitter', 'tweet', targetUrl]);
            }
          });
        }catch(ee){}
      }
      /// -------------------------------------------------------------------------
      /// track facebook likes
      /// -------------------------------------------------------------------------
      /// delay the activation of Facebook sharing as we have to wait for their JS to load
      if( $('#sharefacebook').length && !$("#tcas-ie-bad").length ){
        /// scan wait for Facebook to be ready
        var iid = setInterval(
          function(){
            if( (typeof FB != 'undefined') && FB.Event && FB.Event.subscribe ){
              FB.Event.subscribe('edge.create', function(targetUrl) {
                _gaq.push(['_trackSocial', 'facebook', 'like', targetUrl]);
              });
              FB.Event.subscribe('edge.remove', function(targetUrl) {
                _gaq.push(['_trackSocial', 'facebook', 'unlike', targetUrl]);
              });
              clearInterval(iid); iid = null;
            }
          },
          100
        );
      }
      /// -------------------------------------------------------------------------
      /// track google +
      /// -------------------------------------------------------------------------
      if( $('#sharegoogle').length ){
        /// google + tracking should be implicit
      }
    }
  };

  /// -------------------------------------------------------------------------
  /// TRACK 404
  /// -------------------------------------------------------------------------
  //if( $('.error-404').length ){
    /// we could event track 404's here
    /*
    <script type="text/javascript">
    var gaJsHost = (("https:" == document.location.protocol) ? " https://ssl." : "http://www.");
    document.write(unescape("%3Cscript src='" + gaJsHost + " google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
    </script>
    <script type="text/javascript">
    try{
    var pageTracker = _gat._getTracker("UA-xxxxx-x");
    pageTracker._trackPageview("/404.html?page=" + document.location.pathname + document.location.search + "&from=" + document.referrer);
    } catch(err) {}
    </script>
    */
  //}

  /*
  $('.tate-misc-table').bind('add-new-row.tatemisc', function(){
    var self = $(this);
    alert( self.attr('id') );
  });
  Drupal.theme.tateMiscWarning = function( message, element ){
    $('.tateMisc.warning').remove();
    var add = $('<div class="messages warning tateMisc">' + message + '</div>').fadeTo(0,0);
    element.before(add);
    add.fadeTo(1000,1);
  }
  Drupal.theme.tateMiscListChanged = function( caller_element ){
    var elm = $(caller_element).closest('table');
    if( elm.length ){
      Drupal.theme.tateMiscWarning('Any change (addition, modification or removal) will not be recorded until saved.', elm);
    }
  }
  */

})(jQuery);;
/*
 * Digs out the title from the audio player and appends it in a div beneath it.
 */
(function($) {
  Drupal.behaviors.tate_tour_stop = {
    attach: function(context, settings) {
      $('.jp-playlist').each(function(){
        $(this)
          .hide()
          .find('ul li a:eq(0)')
          .contents()
          .clone()
          .wrap('<li>').parent()
          .wrap('<ul>').parent()
          .wrap('<div class="jp-playlist" />').parent()
          .insertBefore(this)
        ;
      });
    }
  }
})(jQuery);
;
jQuery(function($){
  $('#logo').each(function(){
    var logo = $(this), parts = logo.attr('src').split('_'), logo_number = parseInt(parts[1], 10);
    /// check to see if the format of the url is still as we expect
    if ( !isNaN(logo_number) && (logo_number >= 0) && (logo_number < 5) ) {
      /// add the number back in
      parts[1] = Math.round(Math.random()*4) + '.png';
      /// override the image randomly
      logo.attr('src', parts.join('_'));
    }
  });
});;
(function($){
  var areWeSmallAndMobile = function( pro ){
    var mob = ( null ),
        prs = ( pro && pro.sort ? pro : ['nokia', 'samsung', 'blackberry', 'android', 'ipod', 'iphone'] ),
        len = ( prs.length ),
        uag = ( typeof navigator != 'undefined' ? navigator.userAgent.toLowerCase() : null ),
        win = ( typeof window != 'undefined' ? window : null ),
        doc = ( typeof document != 'undefined' ? document : null ),
        scr = ( typeof screen != 'undefined' ? screen : null ),
        elm = ( doc && doc.getElementsByTagName ? 'getElementsByTagName' : null ),
        bod = ( elm && doc[elm]('body') ? doc[elm]('body')[0] : null ),
        dim = ( scr && scr.availWidth ) || ( win && win.innerWidth ) || ( bod && bod.clientWidth );
    while ( len-- ) { if ( uag.indexOf( prs[len] ) != -1 ) { mob = prs[len]; break; } };
    return !!( mob && (dim < 500) );
  };

  $(function(){
    ///
    var rootElm = $('#js_login_region'), path = rootElm.data('path'), small = areWeSmallAndMobile() || (String(window.location.hash).indexOf('mobile') != -1);
    path && $.ajax({
      url: path,
      success: function( html ) {
        /// jQueryify the html
        $("#js_login_region").html(html);
    	// The 'small' test isn't reliable - at least not for testing on a desktop - so looking for mobile theme class as a back-up
        if (small || $('body').hasClass('tatemobile')) {
        	// Messy hack to make the mobile interface more meaningful
        	var accountLink = $("#js_login_region li.account a");
        	var loginLink = $("#js_login_region li.login a");
        	var mobileAccountHandle = $("#js_login_region").closest('.login-handle').find('.login-handle-title');
        	if (accountLink.length == 1) {
        		var name = accountLink.text();
        		accountLink.text('Edit account');
        		mobileAccountHandle.text(name);
        	} else if (loginLink.length == 1) {
        		var href = loginLink.attr('href');
        		var label = mobileAccountHandle.text();
        		mobileAccountHandle.html('<a href="' + href + '">' + label + '</a>');
        	}
        }
      }
    });

  });

})((typeof jQuery != 'undefined') && jQuery);;
(function($){
  /// Make vertical tabs display require field star
  $(function(){
    $('.vertical-tabs .vertical-tab-button').each(function(){
      var $pane = $(this).closest('.vertical-tabs').find('.vertical-tabs-panes fieldset').eq($(this).index());
      if ( $pane.find('.form-required').length ) {
        $(this).find('a strong').after('<span class="form-required" title="This field is required.">&nbsp;*</span>');
      }
    });
  });
  /// Nicify the CIS show id
  $(function(){
    /// get the base
    var $base = $('#edit-field-cis-show-id');
    /// get and hide the prefix.
    var $prefix = $base.find('.field-prefix'); $prefix.hide();
    /// get the text field
    var $text = $base.find('.form-text');
    /// if we can't find the prefix... don't bother
    if( ! $prefix.length ){ return; };
    /// create a preview button
    var $button = $('<input class="form-submit" type="button" value="Preview Link on CIS" />');
    /// append button
    $text.after( $button );
    /// make the preview functionality work
    $button.click( function(){
      if( ! $text.val() ) return false;
      /// force new window / tab
      window.open( $prefix.text() + $text.val(), '_blank' );
    } );
  });
  /// Repair the lead image field so that it doesn't stretch beyond page width
  $(function(){
    var selector = [];
    selector.push('#edit-field-lead-image .form-autocomplete');
    selector.push('#edit-field-panel-image .form-autocomplete');
    selector.push('#edit-field-panel-image-1 .form-autocomplete');
    selector.push('#edit-field-panel-image-2 .form-autocomplete');
    selector.push('#edit-field-panel-image-3 .form-autocomplete');
    selector.push('#edit-field-panel-image-4 .form-autocomplete');
    selector.push('#edit-field-panel-image-5 .form-autocomplete');
    selector.push('#edit-field-node-related .form-autocomplete');
    $(selector.join(', ')).css({'width':'100%','max-width':'600px'});
  });
  /// Automate the sponsor title creation
  $(function(){
    var output = $('#edit-title');
    var fields = {
      name: $('#edit-field-sponsor-name .form-text'),
      rel: $('#edit-field-sponsor-relationship .form-select')
    };
    if( fields.name.length && fields.rel.length ){
      var update = function(){
        var name = fields.name.val() || '';
        var rel = fields.rel.val() != '_none' ? fields.rel.find('option:selected').text() : '';
        var title = name + ' - ' + rel + '';
        output.val(title);
      }
      fields.name.keyup(update);
      fields.rel.change(update);
    }
  });
  /// Try to clone end dates where appropriate
  $(function(){
    /// list out the items that we want to affect, the following code
    /// targets the date + time fields for a start date and clones
    /// the values across (on blur) to the end date (if it is empty)
    /// the items setup is as follows:
    ///
    /// 1) The key is the jQuery selector for the wrapping parent / parent row
    /// 2) .date-clear will always be targeted as the field
    /// 3) The value should be an array of arrays containing eq() offsets
    ///    In each pair the first offset in the array is the eq() offset
    ///    of the start field. The second number is the eq() offset of the
    ///    destination end field.
    ///    e.g.
    ///    '#edit-field-date-event': [[0,2],[1,3]] means that eq(0) will be
    ///    copied to eq(2), and eq(1) will be copied to eq(3)
    var i, item, items = {
      '#edit-field-date-event': [[0,2],[1,3]],
      '#field-additional-dates-values tr': [[0,2],[1,3]],
      '#edit-field-date-exhibition': [[0,1]],
      '#edit-field-date-series': [[0,1]]
    };
    ///
    var setupListener = function(item, offsets){
      if(offsets && offsets.join && (offsets.length == 2)){
        $(item + ' .date-clear:eq('+offsets[0]+')').live('change', function(){
          var input = $(this);
            var dest = $(item + ' .date-clear:eq('+offsets[1]+')');
            if( !dest.val() ){
              dest.val(input.val());
            }
        });
      }
    }
    ///
    var setupListeners = function( item, offsets ){
      var i, lof = offsets.length, offs = [];
      for(i=0;i<lof;i++){
        if( (offs = offsets[i]) ){
          setupListener( item, offs );
        }
      }
    }
    /// step through each item and apply listeners
    for( i in items ){
      if( (item = items[i]) ){
        setupListeners( i, item );
      }
    }
  });
  /// no conflict
})(jQuery);

(function($) {

/**
 * Drupal FieldGroup object.
 */
Drupal.FieldGroup = Drupal.FieldGroup || {};
Drupal.FieldGroup.Effects = Drupal.FieldGroup.Effects || {};
Drupal.FieldGroup.groupWithfocus = null;

Drupal.FieldGroup.setGroupWithfocus = function(element) {
  element.css({display: 'block'});
  Drupal.FieldGroup.groupWithfocus = element;
}

/**
 * Implements Drupal.FieldGroup.processHook().
 */
Drupal.FieldGroup.Effects.processFieldset = {
  execute: function (context, settings, type) {
    if (type == 'form') {
      // Add required fields mark to any fieldsets containing required fields
      $('fieldset.fieldset', context).once('fieldgroup-effects', function(i) {
        if ($(this).is('.required-fields') && $(this).find('.form-required').length > 0) {
          $('legend span.fieldset-legend', $(this)).eq(0).append(' ').append($('.form-required').eq(0).clone());
        }
        if ($('.error', $(this)).length) {
          $('legend span.fieldset-legend', $(this)).eq(0).addClass('error');
          Drupal.FieldGroup.setGroupWithfocus($(this));
        }
      });
    }
  }
}

/**
 * Implements Drupal.FieldGroup.processHook().
 */
Drupal.FieldGroup.Effects.processAccordion = {
  execute: function (context, settings, type) {
    $('div.field-group-accordion-wrapper', context).once('fieldgroup-effects', function () {
      var wrapper = $(this);

      wrapper.accordion({
        autoHeight: false,
        active: '.field-group-accordion-active',
        collapsible: true,
        changestart: function(event, ui) {
          if ($(this).hasClass('effect-none')) {
            ui.options.animated = false;
          }
          else {
            ui.options.animated = 'slide';
          }
        }
      });

      if (type == 'form') {
        // Add required fields mark to any element containing required fields
        wrapper.find('div.accordion-item').each(function(i){
          if ($(this).is('.required-fields') && $(this).find('.form-required').length > 0) {
            $('h3.ui-accordion-header').eq(i).append(' ').append($('.form-required').eq(0).clone());
          }
          if ($('.error', $(this)).length) {
            $('h3.ui-accordion-header').eq(i).addClass('error');
            var activeOne = $(this).parent().accordion("activate" , i);
            $('.ui-accordion-content-active', activeOne).css({height: 'auto', width: 'auto', display: 'block'});
          }
        });
      }
    });
  }
}

/**
 * Implements Drupal.FieldGroup.processHook().
 */
Drupal.FieldGroup.Effects.processHtabs = {
  execute: function (context, settings, type) {
    if (type == 'form') {
      // Add required fields mark to any element containing required fields
      $('fieldset.horizontal-tabs-pane', context).once('fieldgroup-effects', function(i) {
        if ($(this).is('.required-fields') && $(this).find('.form-required').length > 0) {
          $(this).data('horizontalTab').link.find('strong:first').after($('.form-required').eq(0).clone()).after(' ');
        }
        if ($('.error', $(this)).length) {
          $(this).data('horizontalTab').link.parent().addClass('error');
          Drupal.FieldGroup.setGroupWithfocus($(this));
          $(this).data('horizontalTab').focus();
        }
      });
    }
  }
}

/**
 * Implements Drupal.FieldGroup.processHook().
 */
Drupal.FieldGroup.Effects.processTabs = {
  execute: function (context, settings, type) {
    if (type == 'form') {
      // Add required fields mark to any fieldsets containing required fields
      $('fieldset.vertical-tabs-pane', context).once('fieldgroup-effects', function(i) {
        if ($(this).is('.required-fields') && $(this).find('.form-required').length > 0) {
          $(this).data('verticalTab').link.find('strong:first').after($('.form-required').eq(0).clone()).after(' ');
        }
        if ($('.error', $(this)).length) {
          $(this).data('verticalTab').link.parent().addClass('error');
          Drupal.FieldGroup.setGroupWithfocus($(this));
          $(this).data('verticalTab').focus();
        }
      });
    }
  }
}

/**
 * Implements Drupal.FieldGroup.processHook().
 *
 * TODO clean this up meaning check if this is really
 *      necessary.
 */
Drupal.FieldGroup.Effects.processDiv = {
  execute: function (context, settings, type) {

    $('div.collapsible', context).once('fieldgroup-effects', function() {
      var $wrapper = $(this);

      // Turn the legend into a clickable link, but retain span.field-group-format-toggler
      // for CSS positioning.

      var $toggler = $('span.field-group-format-toggler:first', $wrapper);
      var $link = $('<a class="field-group-format-title" href="#"></a>');
      $link.prepend($toggler.contents());

      // Add required field markers if needed
      if ($(this).is('.required-fields') && $(this).find('.form-required').length > 0) {
        $link.append(' ').append($('.form-required').eq(0).clone());
      }

      $link.appendTo($toggler);

      // .wrapInner() does not retain bound events.
      $link.click(function () {
        var wrapper = $wrapper.get(0);
        // Don't animate multiple times.
        if (!wrapper.animating) {
          wrapper.animating = true;
          var speed = $wrapper.hasClass('speed-fast') ? 300 : 1000;
          if ($wrapper.hasClass('effect-none') && $wrapper.hasClass('speed-none')) {
            $('> .field-group-format-wrapper', wrapper).toggle();
          }
          else if ($wrapper.hasClass('effect-blind')) {
            $('> .field-group-format-wrapper', wrapper).toggle('blind', {}, speed);
          }
          else {
            $('> .field-group-format-wrapper', wrapper).toggle(speed);
          }
          wrapper.animating = false;
        }
        $wrapper.toggleClass('collapsed');
        return false;
      });

    });
  }
};

/**
 * Behaviors.
 */
Drupal.behaviors.fieldGroup = {
  attach: function (context, settings) {
    if (settings.field_group == undefined) {
      return;
    }

    // Execute all of them.
    $.each(Drupal.FieldGroup.Effects, function (func) {
      // We check for a wrapper function in Drupal.field_group as
      // alternative for dynamic string function calls.
      var type = func.toLowerCase().replace("process", "");
      if (settings.field_group[type] != undefined && $.isFunction(this.execute)) {
        this.execute(context, settings, settings.field_group[type]);
      }
    });

    // Fixes css for fieldgroups under vertical tabs.
    $('.fieldset-wrapper .fieldset > legend').css({display: 'block'});
    $('.vertical-tabs fieldset.fieldset').addClass('default-fallback');

  }
};

})(jQuery);;
(function ($) {

/**
 * Toggle the visibility of a fieldset using smooth animations.
 */
Drupal.toggleFieldset = function (fieldset) {
  var $fieldset = $(fieldset);
  if ($fieldset.is('.collapsed')) {
    var $content = $('> .fieldset-wrapper', fieldset).hide();
    $fieldset
      .removeClass('collapsed')
      .trigger({ type: 'collapsed', value: false })
      .find('> legend span.fieldset-legend-prefix').html(Drupal.t('Hide'));
    $content.slideDown({
      duration: 'fast',
      easing: 'linear',
      complete: function () {
        Drupal.collapseScrollIntoView(fieldset);
        fieldset.animating = false;
      },
      step: function () {
        // Scroll the fieldset into view.
        Drupal.collapseScrollIntoView(fieldset);
      }
    });
  }
  else {
    $fieldset.trigger({ type: 'collapsed', value: true });
    $('> .fieldset-wrapper', fieldset).slideUp('fast', function () {
      $fieldset
        .addClass('collapsed')
        .find('> legend span.fieldset-legend-prefix').html(Drupal.t('Show'));
      fieldset.animating = false;
    });
  }
};

/**
 * Scroll a given fieldset into view as much as possible.
 */
Drupal.collapseScrollIntoView = function (node) {
  var h = document.documentElement.clientHeight || document.body.clientHeight || 0;
  var offset = document.documentElement.scrollTop || document.body.scrollTop || 0;
  var posY = $(node).offset().top;
  var fudge = 55;
  if (posY + node.offsetHeight + fudge > h + offset) {
    if (node.offsetHeight > h) {
      window.scrollTo(0, posY);
    }
    else {
      window.scrollTo(0, posY + node.offsetHeight - h + fudge);
    }
  }
};

Drupal.behaviors.collapse = {
  attach: function (context, settings) {
    $('fieldset.collapsible', context).once('collapse', function () {
      var $fieldset = $(this);
      // Expand fieldset if there are errors inside, or if it contains an
      // element that is targeted by the URI fragment identifier.
      var anchor = location.hash && location.hash != '#' ? ', ' + location.hash : '';
      if ($fieldset.find('.error' + anchor).length) {
        $fieldset.removeClass('collapsed');
      }

      var summary = $('<span class="summary"></span>');
      $fieldset.
        bind('summaryUpdated', function () {
          var text = $.trim($fieldset.drupalGetSummary());
          summary.html(text ? ' (' + text + ')' : '');
        })
        .trigger('summaryUpdated');

      // Turn the legend into a clickable link, but retain span.fieldset-legend
      // for CSS positioning.
      var $legend = $('> legend .fieldset-legend', this);

      $('<span class="fieldset-legend-prefix element-invisible"></span>')
        .append($fieldset.hasClass('collapsed') ? Drupal.t('Show') : Drupal.t('Hide'))
        .prependTo($legend)
        .after(' ');

      // .wrapInner() does not retain bound events.
      var $link = $('<a class="fieldset-title" href="#"></a>')
        .prepend($legend.contents())
        .appendTo($legend)
        .click(function () {
          var fieldset = $fieldset.get(0);
          // Don't animate multiple times.
          if (!fieldset.animating) {
            fieldset.animating = true;
            Drupal.toggleFieldset(fieldset);
          }
          return false;
        });

      $legend.append(summary);
    });
  }
};

})(jQuery);
;
(function ($) {

/**
 * Retrieves the summary for the first element.
 */
$.fn.drupalGetSummary = function () {
  var callback = this.data('summaryCallback');
  return (this[0] && callback) ? $.trim(callback(this[0])) : '';
};

/**
 * Sets the summary for all matched elements.
 *
 * @param callback
 *   Either a function that will be called each time the summary is
 *   retrieved or a string (which is returned each time).
 */
$.fn.drupalSetSummary = function (callback) {
  var self = this;

  // To facilitate things, the callback should always be a function. If it's
  // not, we wrap it into an anonymous function which just returns the value.
  if (typeof callback != 'function') {
    var val = callback;
    callback = function () { return val; };
  }

  return this
    .data('summaryCallback', callback)
    // To prevent duplicate events, the handlers are first removed and then
    // (re-)added.
    .unbind('formUpdated.summary')
    .bind('formUpdated.summary', function () {
      self.trigger('summaryUpdated');
    })
    // The actual summaryUpdated handler doesn't fire when the callback is
    // changed, so we have to do this manually.
    .trigger('summaryUpdated');
};

/**
 * Sends a 'formUpdated' event each time a form element is modified.
 */
Drupal.behaviors.formUpdated = {
  attach: function (context) {
    // These events are namespaced so that we can remove them later.
    var events = 'change.formUpdated click.formUpdated blur.formUpdated keyup.formUpdated';
    $(context)
      // Since context could be an input element itself, it's added back to
      // the jQuery object and filtered again.
      .find(':input').andSelf().filter(':input')
      // To prevent duplicate events, the handlers are first removed and then
      // (re-)added.
      .unbind(events).bind(events, function () {
        $(this).trigger('formUpdated');
      });
  }
};

/**
 * Prepopulate form fields with information from the visitor cookie.
 */
Drupal.behaviors.fillUserInfoFromCookie = {
  attach: function (context, settings) {
    $('form.user-info-from-cookie').once('user-info-from-cookie', function () {
      var formContext = this;
      $.each(['name', 'mail', 'homepage'], function () {
        var $element = $('[name=' + this + ']', formContext);
        var cookie = $.cookie('Drupal.visitor.' + this);
        if ($element.length && cookie) {
          $element.val(cookie);
        }
      });
    });
  }
};

})(jQuery);
;
