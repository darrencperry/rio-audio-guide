<html lang="en" dir="ltr" prefix="content: http://purl.org/rss/1.0/modules/content/ dc: http://purl.org/dc/terms/ foaf: http://xmlns.com/foaf/0.1/ og: http://ogp.me/ns# rdfs: http://www.w3.org/2000/01/rdf-schema# sioc: http://rdfs.org/sioc/ns# sioct: http://rdfs.org/sioc/types# skos: http://www.w3.org/2004/02/skos/core# xsd: http://www.w3.org/2001/XMLSchema#" class="js">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta content="Devonport Guildhall and Column Audio Tour" about="/tate-britain-mobile/tour/introduction" property="dc:title">
    <meta name="description" content="Devonport Guildhall and Column Audio Tour">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">
    <!--<link rel="shortcut icon" href="http://www.tate.org.uk/favicon.ico" type="image/vnd.microsoft.icon">-->
    <title>Devonport Guildhall and Column Audio Tour</title>
    <link type="text/css" rel="stylesheet" href="css/jplayer.css" media="all">
    <link type="text/css" rel="stylesheet" href="css/main.css" media="all">

    <!--[if lte IE 9]>
    <link type="text/css" rel="stylesheet" href="css/ie-9.css" media="all" />
    <![endif]-->

    <!--[if lte IE 8]>
    <link type="text/css" rel="stylesheet" href="css/ie-8.css" media="all" />
    <![endif]-->

    <!--[if lte IE 7]>
    <link type="text/css" rel="stylesheet" href="css/ie-7.css" media="all" />
    <![endif]-->

    <!--[if (lt IE 9)&(!IEMobile)]>
    <link type="text/css" rel="stylesheet" href="css/ie9nomobile.css" media="all"/>
    <![endif]-->

    <!--[if gte IE 9]><!-->
    <link type="text/css" rel="stylesheet" href="css/grid.css" media="all">
    <!--<![endif]-->

    <script type="text/javascript" src="js/jquery.min.js"></script>
    <style type="text/css"></style>
    <script type="text/javascript">
        <!--//--><![CDATA[//><!--
        window.jQuery || document.write("<script src='js/jquery.min.js'>\x3C/script>")
        //--><!]]>
    </script>
    <script type="text/javascript" src="js/jquery.once.js"></script>
    <script type="text/javascript" src="js/jquery.jplayer.js"></script>
    <script type="text/javascript" src="js/jquery.cookie.js"></script>
    <script type="text/javascript">
        <!--//--><![CDATA[//><!--
        //        jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"tatemobile","theme_token":"j_-rCQbI8syVpxgIpCCxDar3J29wSUYaa-gr0Sewq3A","js":{"sites\/all\/modules\/jplayer\/theme\/jplayer.js":1,"https:\/\/ajax.googleapis.com\/ajax\/libs\/jquery\/1.5.2\/jquery.min.js":1,"0":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/libraries\/jplayer\/jquery.jplayer.min.js":1,"sites\/all\/modules\/tate_comment_and_share\/js\/comments.js":1,"sites\/all\/modules\/tate_comment_and_share\/js\/cacheless-messages.js":1,"sites\/all\/libraries\/colorbox\/jquery.colorbox-min.js":1,"sites\/all\/modules\/colorbox\/js\/colorbox.js":1,"sites\/all\/modules\/colorbox\/js\/colorbox_load.js":1,"sites\/all\/modules\/hint\/hint.js":1,"sites\/all\/modules\/tate_misc\/tate_misc_ga.js":1,"sites\/all\/modules\/tate_tours\/js\/tate_tour_stop.js":1,"sites\/all\/modules\/tate_blocks\/tate_blocks_logo_random.js":1,"sites\/all\/modules\/tate_blocks\/tate_blocks_login_choice.js":1,"sites\/all\/modules\/tate_misc\/tate_misc.js":1,"sites\/all\/modules\/field_group\/field_group.js":1,"misc\/collapse.js":1,"misc\/form.js":1,"sites\/all\/themes\/tatemobile\/js\/jquery.cookie.js":1,"sites\/all\/themes\/tatemobile\/js\/jquery.fitvids.js":1,"sites\/all\/themes\/tatemobile\/js\/core.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.theme.css":1,"sites\/all\/modules\/comment_notify\/comment_notify.css":1,"sites\/all\/modules\/footnotes\/footnotes.css":1,"modules\/book\/book.css":1,"modules\/comment\/comment.css":1,"sites\/all\/modules\/date\/date_api\/date.css":1,"sites\/all\/modules\/date\/date_popup\/themes\/datepicker.1.7.css":1,"sites\/all\/modules\/date\/date_repeat_field\/date_repeat_field.css":1,"modules\/field\/theme\/field.css":1,"sites\/all\/modules\/logintoboggan\/logintoboggan.css":1,"sites\/all\/modules\/modified\/mollom\/mollom.css":1,"modules\/node\/node.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/typogrify\/typogrify.css":1,"sites\/all\/themes\/tatebase\/css\/tours.css":1,"sites\/all\/modules\/jplayer\/theme\/jplayer.css":1,"sites\/all\/modules\/field_group\/field_group.css":1,"sites\/all\/themes\/tatemobile\/css\/global.css":1,"sites\/all\/themes\/tatemobile\/css\/colorbox.css":1,"sites\/all\/themes\/tatemobile\/css\/mobile.css":1,"sites\/all\/modules\/tate_comment_and_share\/css\/rss.css":1,"sites\/all\/modules\/tate_comment_and_share\/css\/comments.css":1,"sites\/all\/themes\/tatemobile\/css\/ie-9.css":1,"sites\/all\/themes\/tatemobile\/css\/ie-8.css":1,"sites\/all\/themes\/tatemobile\/css\/ie-7.css":1,"sites\/all\/themes\/tatemobile\/css\/print.css":1,"ie::normal::sites\/all\/themes\/omega\/alpha\/css\/grid\/alpha_default\/normal\/alpha-default-normal-12.css":1,"sites\/all\/themes\/omega\/alpha\/css\/grid\/alpha_default\/narrow\/alpha-default-narrow-12.css":1,"sites\/all\/themes\/omega\/alpha\/css\/grid\/alpha_default\/normal\/alpha-default-normal-12.css":1,"sites\/all\/themes\/omega\/alpha\/css\/grid\/alpha_default\/wide\/alpha-default-wide-12.css":1}},"colorbox":{"transition":"elastic","speed":"200","opacity":"0.60","slideshow":false,"slideshowAuto":true,"slideshowSpeed":"2500","slideshowStart":"start slideshow","slideshowStop":"stop slideshow","current":"{current} of {total}","previous":"\u00ab Prev","next":"Next \u00bb","close":"Close","overlayClose":true,"maxWidth":"100%","maxHeight":"100%","initialWidth":"300","initialHeight":"250","fixed":true,"scrolling":true},"jcarousel":{"ajaxPath":"\/jcarousel\/ajax\/views"},"tateMisc":{"items":[{"selector":"$(\u0022.tate-panel h3 a\u0022);","interaction":"click","category":"","category_value":"PageURL","action":"","action_value":"Title","title":"Masonry Panel Links","title_value":"","position":"1"},{"selector":"$(\u0022.see-also-panel\u0022).find(\u0022h3 a, .action_button a\u0022);","interaction":"click","category":"","category_value":"PageURL","action":"","action_value":"NearestTitle","title":"RHS Links","title_value":"","position":"2"},{"selector":"$(\u0022.see-also-link a\u0022);","interaction":"click","category":"","category_value":"PageURL","action":"","action_value":"Text","title":"See Also Links","title_value":"","position":"3"},{"selector":"$(\u0022a[href*=\u0027download\/\u0027],a[href*=\u0027file\/\u0027]\u0022);","interaction":"click","category":"","category_value":"LinkFileType","action":"","action_value":"Link","title":"Download Links","title_value":"","position":"4"},{"selector":"$(\u0022a[href*=\u0027:\/\/\u0027]:not(a[href*=\u0027:\/\/auth.tate.org.uk\/\u0027],a[href*=\u0027tate.org.uk\/\u0027],.twitter-share-button)\u0022);","interaction":"click","category":"","category_value":"PageURL","action":"","action_value":"Link","title":"External Links","title_value":"","position":"5"},{"selector":"$(\u0022.block-tate-blocks-footer-bulletin-form form\u0022);","interaction":"submit","category":"","category_value":"PageURL","action":"Email Sign-up","action_value":"","title":"Forms","title_value":"","position":"6"},{"selector":"$(\u0027#main-menu a\u0027);","interaction":"click","category":"Navigation","category_value":"","action":"Main menu","action_value":"","title":"","title_value":"Link","position":"7"},{"selector":"$(\u0022.tate-panel h3 a\u0022);","interaction":"click","category":"Navigation","category_value":"","action":"Masonry Panel Links","action_value":"","title":"","title_value":"Link","position":"8"},{"selector":"$(\u0022.see-also-panel\u0022).find(\u0022h3 a, .action_button a\u0022);","interaction":"click","category":"Navigation","category_value":"","action":"See Also Panels","action_value":"","title":"","title_value":"Link","position":"9"},{"selector":"$(\u0022.see-also-link a\u0022);","interaction":"click","category":"Navigation","category_value":"","action":"See Also Links","action_value":"","title":"","title_value":"Link","position":"10"},{"selector":"$(\u0027#secondary-menu a\u0027);","interaction":"click","category":"Navigation","category_value":"","action":"Secondary Menu","action_value":"","title":"","title_value":"Link","position":"11"},{"selector":"$(\u0027#breadcrumb a\u0027);","interaction":"click","category":"Navigation","category_value":"","action":"Breadcrumbs","action_value":"","title":"","title_value":"Link","position":"12"},{"selector":"$(\u0027#footer a\u0027);","interaction":"click","category":"Navigation","category_value":"","action":"Footer","action_value":"","title":"","title_value":"Link","position":"13"},{"selector":"$(\u0027a.tate-logo\u0027);","interaction":"click","category":"Navigation","category_value":"","action":"Logo","action_value":"","title":"","title_value":"Link","position":"14"},{"selector":"$(\u0027#region-sidebar-first .block-tate-facet a\u0027);","interaction":"click","category":"Navigation","category_value":"","action":"Facets","action_value":"","title":"","title_value":"Link","position":"15"},{"selector":"$(\u0027#region-sidebar-first .block-menu-block a\u0027);","interaction":"click","category":"Navigation","category_value":"","action":"LHS Menu","action_value":"","title":"","title_value":"Link","position":"16"},{"selector":"$(\u0027.menu-name-menu-galleries a\u0027);","interaction":"click","category":"Navigation","category_value":"","action":"Gallery Menu","action_value":"","title":"","title_value":"Link","position":"17"},{"selector":"$(\u0027.block-ddblock a\u0027);","interaction":"click","category":"Navigation","category_value":"","action":"Banner","action_value":"","title":"","title_value":"Link","position":"18"},{"selector":"$(\u0027.tags-wrapper a\u0027);","interaction":"click","category":"Navigation","category_value":"","action":"Tags","action_value":"","title":"","title_value":"Text","position":"19"},{"selector":"$(\u0027.block-apachesolr-search a\u0027);","interaction":"click","category":"Navigation","category_value":"","action":"More Like This","action_value":"","title":"","title_value":"Link","position":"20"},{"selector":"$(\u0027#sharetwitter\u0027);","interaction":"click","category":"Social Media","category_value":"","action":"Twitter","action_value":"","title":"","title_value":"PageURL","position":"21"},{"selector":"$(\u0027.block-tatesharebuttons\u0027);","interaction":"click","category":"Social Media","category_value":"","action":"All","action_value":"","title":"","title_value":"PageURL","position":"22"},{"selector":"$(\u0027a.nav-link\u0027);","interaction":"click","category":"Interface","category_value":"","action":"RSS","action_value":"","title":"","title_value":"Link","position":"23"},{"selector":"$(\u0027.jcarousel-next\u0027);","interaction":"click","category":"Interface","category_value":"","action":"Carousel","action_value":"","title":"Next","title_value":"","position":"24"},{"selector":"$(\u0027.jcarousel-prev\u0027);","interaction":"click","category":"Interface","category_value":"","action":"Carousel","action_value":"","title":"Previous","title_value":"","position":"25"}],"icons":{"pp":"sites\/all\/themes\/tatebase\/images\/icons\/icon_pp.png","excel":"sites\/all\/themes\/tatebase\/images\/icons\/icon_excel.png","wav":"sites\/all\/themes\/tatebase\/images\/icons\/icon_wav.png","audio":"sites\/all\/themes\/tatebase\/images\/icons\/icon_audio.png","file":"sites\/all\/themes\/tatebase\/images\/icons\/icon_file.png","img":"sites\/all\/themes\/tatebase\/images\/icons\/icon_img.png","mp3":"sites\/all\/themes\/tatebase\/images\/icons\/icon_mp3.png","zip":"sites\/all\/themes\/tatebase\/images\/icons\/icon_zip.png","rtf":"sites\/all\/themes\/tatebase\/images\/icons\/icon_rtf.png","msword":"sites\/all\/themes\/tatebase\/images\/icons\/icon_msword.png","doc":"sites\/all\/themes\/tatebase\/images\/icons\/icon_doc.png","word":"sites\/all\/themes\/tatebase\/images\/icons\/icon_word.png","pdf":"sites\/all\/themes\/tatebase\/images\/icons\/icon_pdf.png","video":"sites\/all\/themes\/tatebase\/images\/icons\/icon_video.png"}},"tate_comment_and_share":{"comment-and-share-form-75101":{"form_html":"","output_id":"comment-and-share-form-75101","nid":"158072"},"comment-and-share-form-84513":{"form_html":"","output_id":"comment-and-share-form-84513","nid":"287889"}},"tate_comment_and_share__messages":{"comment.moderated":"Comments with links are reviewed by a moderator and will be published after approval."},"field_group":{"div":"lead_media_8_col","htabs":"lead_media_8_col"},"jplayerInstances":
        //        {
        //            "jplayer-node-287889-field-audio-url-1422299207":{
        //                "files":[
        //                    {
        //                        "mp3":"http:\/\/static.tate.org.uk\/1\/mobile\/tbcollection\/Stop100_web.mp3"
        //                    }
        //                ],
        //                "solution":"html, flash",
        //                "supplied":"mp3",
        //                "preload":"metadata",
        //                "volume":0.8,
        //                "muted":false,
        //                "autoplay":false,
        //                "repeat":"none",
        //                "backgroundColor":"000000"
        //            }
        //        },
        //        "jPlayer":{"swfPath":"\/sites\/all\/libraries\/jplayer","showHour":true,"showMin":true,"showSec":true,"padHour":false,"padMin":true,"padSec":true,"protected":0,"sepHour":":","sepMin":":","sepSec":""}});
        //--><!]]>
    </script>

    <script>
        var images = [<?php foreach($imageurl as $key=>$value)
        {
            echo '"'.$value.'"';
            if($key != count($imageurl)-1) echo ',';
        } ?>];

        jQuery(function($){

            var slideshow = $("#slideshow");
            var slideshowCount = 0;
            setInterval(slideshowImageChange, 5000);

            function slideshowImageChange()
            {
                slideshow.attr('src', images[slideshowCount]);
                slideshowCount++;
                if(slideshowCount >= images.length) slideshowCount = 0;
            }
        });




    </script>
    <!--[if lt IE 9]><script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <style type="text/css" id="ki-bud-bo"></style>
    <style type="text/css" id="ki-bud-bt"></style>
</head>

<body class="html not-front not-logged-in page-node page-node- page-node-288029 node-type-tour-stop context-tate-britain-mobile no-sidebars tatemobile">
<div id="page-wrapper">
    <div id="page">


        <div id="header">
            <img src="img/header.gif" />
        </div>
        <!-- /.section, /#header -->