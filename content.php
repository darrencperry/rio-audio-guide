<script>
    var playerSettings = {
        "files": [
            {
                "mp3": "<?php echo $m4aurl; ?>"
            }
        ],
        "solution": "html, flash",
        "supplied": "mp3",
        "preload": "metadata",
        "volume": 0.8,
        "muted": false,
        "autoplay": false,
        "repeat": "none",
        "backgroundColor": "000000"
    };
</script>

<?php include_once('header.php'); ?>

<div id="main-wrapper">
    <div id="main" class="clearfix">
        <div id="content" class="column">
            <div class="section">
                <section id="section-content" class="section section-content">
                    <div class="jp-audio">
                        <div class="jp-type-playlist">

                            <div id="jplayer-node-287889-field-audio-url-1422299207" class="jp-jplayer" style="background-color: rgb(0, 0, 0);">
                                <audio id="jp_audio_0" preload="metadata" src="<?php echo $m4aurl; ?>"></audio>
                            </div>

                            <!-- Image -->
                            <div class="above-jplayer"><?php if($imageurl): ?><img id="slideshow" src="<?php echo $imageurl[0]; ?>"/>
                            <?php endif; ?></div>
                            <!-- /Image -->

                            <!-- JPlayer -->
                            <div id="jplayer-node-287889-field-audio-url-1422299207_interface" class="jp-interface">
                                <ul class="jp-controls playpause">
                                    <li><a href="#" class="jp-play" tabindex="1">play</a>
                                    </li>
                                    <li><a href="#" class="jp-pause" tabindex="1" style="display: none;">pause</a>
                                    </li>
                                    <!--li><a href="#" class="jp-stop" tabindex="1">stop</a></li-->
                                </ul>

                                <div class="jp-rhscol">

                                    <div class="jp-progress">
                                        <div class="jp-seek-bar" style="width: 100%;">
                                            <div class="jp-play-bar" style="width: 0%;"></div>
                                        </div>
                                    </div>

                                    <div class="jp-rhscol-audio">

                                        <ul class="jp-controls">
                                            <li><a href="#" class="jp-mute" tabindex="1">mute</a>
                                            </li>
                                            <li><a href="#" class="jp-unmute" tabindex="1" style="display: none;">unmute</a>
                                            </li>
                                        </ul>

                                        <div class="jp-volume-bar">
                                            <div class="jp-volume-bar-value" style="width: 80%;"></div>
                                        </div>

                                    </div>

                                    <div class="jp-time">
                                        <div class="jp-current-time">0:00:00</div> /
                                        <div class="jp-duration">0:00:00</div>
                                    </div>

                                </div>
                            </div>
                            <!-- /JPlayer -->

                            <!-- Description -->
                            <div class="jp-playlist">
                                <?php echo $titlehtml; ?>
                            </div>
                            <!-- /Description -->

                            <!-- Nav -->
                            <div class="below-jplayer">
                                <div class="nav-control prev">
                                    <a href="<?php echo $prevurl; ?>">
                                        <img src="img/left.gif">Back
                                    </a>
                                </div>
                                <div class="nav-control home">
                                    <a href="index.php">
                                        <img src="img/home.gif">
                                    </a>
                                </div>
                                <div class="nav-control next">
                                    <a href="<?php echo $nexturl; ?>">
                                        Next<img src="img/right.gif">
                                    </a>
                                </div>
                            </div>
                            <!-- /Nav -->

                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- /.section, /#content -->



    </div>
</div>

<?php include_once('footer.php'); ?>